Ext.define("ExtThemeNeptune.Component", {
    override: "Ext.Component",
    initComponent: function () {
        this.callParent();
        if (this.dock && this.border === undefined) {
            this.border = false;
        }
    },
    initStyles: function () {
        var b = this,
            a = b.border;
        if (b.dock) {
            b.border = null;
        }
        b.callParent(arguments);
        b.border = a;
    }
});
Ext.define("ExtThemeNeptune.panel.Panel", {
    override: "Ext.panel.Panel",
    border: false,
    bodyBorder: false,
    initBorderProps: Ext.emptyFn,
    initBodyBorder: function () {
        if (this.bodyBorder !== true) {
            this.callParent();
        }
    }
});
Ext.define("ExtThemeNeptune.window.Window", {
    override: "Ext.window.Window",
    shadow: false
});
Ext.define("ExtThemeNeptune.layout.component.Dock", {
    override: "Ext.layout.component.Dock",
    noBorderClassTable: [0, Ext.baseCSSPrefix + "noborder-l", Ext.baseCSSPrefix + "noborder-b", Ext.baseCSSPrefix + "noborder-bl", Ext.baseCSSPrefix + "noborder-r", Ext.baseCSSPrefix + "noborder-rl", Ext.baseCSSPrefix + "noborder-rb", Ext.baseCSSPrefix + "noborder-rbl", Ext.baseCSSPrefix + "noborder-t", Ext.baseCSSPrefix + "noborder-tl", Ext.baseCSSPrefix + "noborder-tb", Ext.baseCSSPrefix + "noborder-tbl", Ext.baseCSSPrefix + "noborder-tr", Ext.baseCSSPrefix + "noborder-trl", Ext.baseCSSPrefix + "noborder-trb", Ext.baseCSSPrefix + "noborder-trbl"],
    edgeMasks: {
        top: 8,
        right: 4,
        bottom: 2,
        left: 1
    },
    handleItemBorders: function () {
        var y = this,
            f = 0,
            z = 8,
            A = 4,
            l = 2,
            e = 1,
            a = y.owner,
            s = a.bodyBorder,
            n = a.border,
            j = y.collapsed,
            p = y.edgeMasks,
            k = y.noBorderClassTable,
            x = a.dockedItems.generation,
            w, d, v, h, r, m, u, o, g, q, t, c;
        if (y.initializedBorders === x) {
            return;
        }
        t = [];
        c = [];
        d = y.getBorderCollapseTable();
        k = y.getBorderClassTable ? y.getBorderClassTable() : k;
        y.initializedBorders = x;
        y.collapsed = false;
        v = y.getDockedItems();
        y.collapsed = j;
        for (r = 0, m = v.length; r < m; r++) {
            u = v[r];
            if (u.ignoreBorderManagement) {
                continue;
            }
            o = u.dock;
            q = h = 0;
            t.length = 0;
            c.length = 0;
            if (o !== "bottom") {
                if (f & z) {
                    w = u.border;
                } else {
                    w = n;
                    if (w !== false) {
                        h += z;
                    }
                }
                if (w === false) {
                    q += z;
                }
            }
            if (o !== "left") {
                if (f & A) {
                    w = u.border;
                } else {
                    w = n;
                    if (w !== false) {
                        h += A;
                    }
                }
                if (w === false) {
                    q += A;
                }
            }
            if (o !== "top") {
                if (f & l) {
                    w = u.border;
                } else {
                    w = n;
                    if (w !== false) {
                        h += l;
                    }
                }
                if (w === false) {
                    q += l;
                }
            }
            if (o !== "right") {
                if (f & e) {
                    w = u.border;
                } else {
                    w = n;
                    if (w !== false) {
                        h += e;
                    }
                }
                if (w === false) {
                    q += e;
                }
            }
            if ((g = u.lastBorderMask) !== q) {
                u.lastBorderMask = q;
                if (g) {
                    c[0] = k[g];
                }
                if (q) {
                    t[0] = k[q];
                }
            }
            if ((g = u.lastBorderCollapse) !== h) {
                u.lastBorderCollapse = h;
                if (g) {
                    c[c.length] = d[g];
                }
                if (h) {
                    t[t.length] = d[h];
                }
            }
            if (c.length) {
                u.removeCls(c);
            }
            if (t.length) {
                u.addCls(t);
            }
            f |= p[o];
        }
        q = h = 0;
        t.length = 0;
        c.length = 0;
        if (f & z) {
            w = s;
        } else {
            w = n;
            if (w !== false) {
                h += z;
            }
        }
        if (w === false) {
            q += z;
        }
        if (f & A) {
            w = s;
        } else {
            w = n;
            if (w !== false) {
                h += A;
            }
        }
        if (w === false) {
            q += A;
        }
        if (f & l) {
            w = s;
        } else {
            w = n;
            if (w !== false) {
                h += l;
            }
        }
        if (w === false) {
            q += l;
        }
        if (f & e) {
            w = s;
        } else {
            w = n;
            if (w !== false) {
                h += e;
            }
        }
        if (w === false) {
            q += e;
        }
        if ((g = y.lastBodyBorderMask) !== q) {
            y.lastBodyBorderMask = q;
            if (g) {
                c[0] = k[g];
            }
            if (q) {
                t[0] = k[q];
            }
        }
        if ((g = y.lastBodyBorderCollapse) !== h) {
            y.lastBodyBorderCollapse = h;
            if (g) {
                c[c.length] = d[g];
            }
            if (h) {
                t[t.length] = d[h];
            }
        }
        if (c.length) {
            a.removeBodyCls(c);
        }
        if (t.length) {
            a.addBodyCls(t);
        }
    },
    onRemove: function (b) {
        var a = b.lastBorderMask;
        if (!b.isDestroyed && !b.ignoreBorderManagement && a) {
            b.lastBorderMask = 0;
            b.removeCls(this.noBorderClassTable[a]);
        }
        this.callParent([b]);
    }
});
Ext.define("ExtThemeNeptune.toolbar.Toolbar", {
    override: "Ext.toolbar.Toolbar",
    usePlainButtons: false,
    border: false
});
Ext.define("ExtThemeNeptune.container.ButtonGroup", {
    override: "Ext.container.ButtonGroup",
    usePlainButtons: false
});
Ext.define("ExtThemeNeptune.toolbar.Paging", {
    override: "Ext.toolbar.Paging",
    defaultButtonUI: "plain-toolbar",
    inputItemWidth: 40
});
Ext.define("ExtThemeNeptune.picker.Month", {
    override: "Ext.picker.Month",
    measureMaxHeight: 36
});
Ext.define("ExtThemeNeptune.form.field.HtmlEditor", {
    override: "Ext.form.field.HtmlEditor",
    defaultButtonUI: "plain-toolbar"
});
Ext.define("ExtThemeNeptune.panel.Table", {
    override: "Ext.panel.Table",
    bodyBorder: true
});
Ext.define("ExtThemeNeptune.grid.RowEditor", {
    override: "Ext.grid.RowEditor",
    buttonUI: "default-toolbar"
});
Ext.define("ExtThemeNeptune.grid.column.RowNumberer", {
    override: "Ext.grid.column.RowNumberer",
    width: 25
});
Ext.define("ExtThemeNeptune.resizer.Splitter", {
    override: "Ext.resizer.Splitter",
    size: 8
});
Ext.define("ExtThemeNeptune.menu.Menu", {
    override: "Ext.menu.Menu",
    showSeparator: false
});
Ext.define("ExtThemeNeptune.menu.Separator", {
    override: "Ext.menu.Separator",
    border: true
});
Ext.define("ExtThemeNeptune.panel.Tool", {
    override: "Ext.panel.Tool",
    height: 16,
    width: 16
});
Ext.define("ExtThemeNeptune.tab.Tab", {
    override: "Ext.tab.Tab",
    border: false
});
Ext.define("ExtThemeNeptune.picker.Color", {
    override: "Ext.picker.Color",
    colors : [
        '1983FB', '1373F0', '1064DE', '0D55CC', '0A47BA', '0739A8', '052C97', '04248C', '032081', '03207C',
        '38D927', '33C723', '2EB51F', '29A31B', '259317', '208213', '1C720F', '18620C', '145308', '134E07',
        'B326FB', 'A324F1', '9225FB', '821DCC', '7319BA', '6415A8', '551297', '511192', '4D108C', '480F87',
        'F5178C', 'E3137B', 'D2106C', 'C10D5D', 'B00A4E', '9F0840', '99073B', '940637', '8E0633', '89052F',
        'FA8D25', 'E77D21', 'D56D1C', 'C35D18', 'B14E14', 'AB4A13', 'A64512', 'A04010', '9A3B0F', '953C0F',
        '000000', '333333', '666666', 'BBBBBB', 'DDDDDD', 'FFFFFF', 'FC0D1B', 'FD7F23', 'FFFD38', '84FD31',
        'D13B1F', 'F60D1A', 'FBB32B', 'FFFD38', '87C93B', '29A33C', '24A2EF', '105DB5', '02174F', '582092',
        'F4CECE', 'FDB8B9', 'F8DFCA', 'FEECC4', 'EEEEC5', 'D5E8C8', 'D7E6F4', 'CBDBE9', '35C3FC', 'F684C1',
        'DE806D', 'FC5E61', 'EDA06F', 'F9D454', 'CCD55D', '9CC878', '8DB7E0', '7C99D5', '8CC2FD', 'F228FC',
        '5C1B0D', 'AC060F', '6D2C08', '6C4F0D', '424908', '2D4518', '193D67', '182A53', '2A56FB', '630963'
    ]
});