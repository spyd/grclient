require "resolv"
require "net/http"

class Adapter < ActiveRecord::Base
  attr_accessible :description, :host, :name, :port

  validates :host,
            presence: true,
            uniqueness: true,
            format: { with: Resolv::IPv4::Regex }

  validates :port,
            presence: true,
            numericality: true

  def get_data(params)
    params.delete(:controller)
    params.delete(:action)
    url = "http://#{self.host}:#{self.port}#{params.delete('resource')}"

    if url =~ /\?/
      url += "&"
    else
      url += "?"
    end

    url += (params.map { |k, v| "#{k}=#{v}"}).join("&")
    json = Net::HTTP.get_response(URI.parse(url)).body
    return ActiveSupport::JSON.decode(json)
  end
end
