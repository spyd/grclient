require "resolv"
require "net/http"

class AdaptersController < ApplicationController
  skip_before_filter :verify_authenticity_token
  def index
  end

  def data
    @adapter = Adapter.find(1)
    resp = {}
    begin
      data = @adapter.get_data(params)
      resp = {success: true, data: data}
    rescue Exception => e
      resp = {success: false, data: e}
    end
    render json: resp
  end

  def get
    @adapter = getAdapter(params)
    req_data = convert_params(true)
    req = Net::HTTP::Get.new("/#{req_data[:resource]}", content)
    resp = perform req
    render json: resp
  end

  def post
    @adapter = getAdapter(params)
    req_data = convert_params
    req = Net::HTTP::Post.new("/#{req_data[:resource]}", content)
    req.body = req_data[:body]
    resp = perform req
    render json: resp
  end

  def put
    @adapter = getAdapter(params)
    req_data = convert_params
    req = Net::HTTP::Put.new("/#{req_data[:resource]}", content)
    req.body = req_data[:body]
    resp = perform req

    render json: resp
  end

  def delete
    @adapter = getAdapter(params)
    req_data = convert_params
    req = Net::HTTP::Delete.new("/#{req_data[:resource]}", content)
    resp = perform req

    render json: resp
  end

  private

    def getAdapter(params)
      Adapter.find(params[:adapter_id].to_i)
    end

    def perform(req)
      begin
        resp = Net::HTTP.new(@adapter.host, @adapter.port).start do |http|
          http.request(req)
        end
        #Rails.logger.warn("Attempted to Request: #{req.body}")
        data = ActiveSupport::JSON.decode(resp.body)
        #data = ActiveSupport::JSON.decode(resp.body)
        #rescue ActiveSupport::JSON.parse_error
        #Rails.logger.warn("Attempted to decode invalid JSON jhv: #{resp.body}")
        { success: true, data: data }
      rescue Exception => e
        { success: true, data: e.to_s }
      end
    end

    def convert_params(withParams = false)
      request_data = {}
      request_data[:resource] = resource_path params
      delete_unused_params
      if (withParams)
        if request_data[:resource] =~ /\?/
          request_data[:resource] += "&"
        else
          request_data[:resource] += "?"
        end
        request_data[:resource] += URI.encode_www_form(params)
      else
        params.delete('adapter')
        request_data[:body] = params.to_json
        #Rails.logger.warn("Hello JSON: #{params.to_json}")
      end
      request_data
    end

    def delete_unused_params
      params.delete(:controller)
      params.delete(:action)
      params.delete(:adapter_id)
      params.delete(:resource)
      params.delete_if { |k,v| v.nil? }
    end

    def content
      { 'Content-Type' => 'application/json' }
    end

    def resource_path(req_data)
      path_url = ""
      Rails.logger.warn("Hello JSON: #{req_data[:resource]}")
      if "/#{req_data[:resource]}" == "/spyd/accessories/0"
        path_url = "/spyd/accessories"
      elsif "/#{req_data[:resource]}" == "/spyd/loans/0"
        path_url = "/spyd/loans"
      elsif "/#{req_data[:resource]}" == "/spyd/reserves/0"
        path_url = "/spyd/reserves"
      else
        path_url = "/#{req_data[:resource]}"
      end
      path_url
    end
end
