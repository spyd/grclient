/**
 * @class Spyd.model.AccessoryView
 * @extends Ext.data.Model
 * AccessoryView model difinition.
 */
Ext.define('Spyd.model.AccessoryView', {
    extend: 'Ext.data.Model',

    fields: [{
        name: 'AccessoryId',
        type: 'int'
    }, {
        name: 'LoanId',
        type: 'int'
    }, {
        name: 'AccessoryName',
        type: 'string'
    }, {
        name: 'EmployeeName',
        type: 'string'
    }, {
        name: 'LoanDate',
        type: 'string'
    }, {
        name: 'LoanHour',
        type: 'string'
    }, {
        name: 'NextReserveDate',
        type: 'date'
    }, {
        name: 'NextReserveHour',
        type: 'string'
    }, {
        name: 'Status',
        type: 'string'
    }]
});
