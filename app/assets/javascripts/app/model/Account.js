/**
 * @class Spyd.model.Account
 * @extends Ext.data.Model
 * Account model difinition.
 */
Ext.define('Spyd.model.Account', {
    extend: 'Ext.data.Model',

    fields: [{
        name: 'EmployeeId',
        type: 'int'
    }, {
        name: 'UserAccount',
        type: 'string'
    }, {
        name: 'Password',
        type: 'string'
    }, {
        name: 'RoleId',
        type: 'int'
    }],

    idProperty: 'UserAccount',
});
