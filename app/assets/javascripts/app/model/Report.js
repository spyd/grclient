/**
 * @class Spyd.model.Report
 * @extends Ext.data.Model
 * Report model difinition.
 */
Ext.define('Spyd.model.Report', {
    extend: 'Ext.data.Model',

    fields: [{
        name: 'ReportId',
        type: 'int'
    }, {
        name: 'ReportName',
        type: 'string'
    }],

    idProperty: 'ReportId'
});
