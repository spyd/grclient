/**
 * @class Spyd.model.Role
 * @extends Ext.data.Model
 * Role model difinition.
 */
Ext.define('Spyd.model.Role', {
    extend: 'Ext.data.Model',

    fields: [{
        name: 'RoleId',
        type: 'int'
    }, {
        name: 'RoleName',
        type: 'string'
    }],

    idProperty: 'RoleId'
});
