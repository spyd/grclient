/**
 * @class Spyd.model.State
 * @extends Ext.data.Model
 * State model difinition.
 */
Ext.define('Spyd.model.State', {
    extend: 'Ext.data.Model',

    fields: [{
        name: 'StateId',
        type: 'int'
    }, {
        name: 'StateName',
        type: 'string'
    }, {
        name: 'Visibility',
        type: 'bool'
    }],

    idProperty: 'StateId'
});
