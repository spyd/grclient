/**
 * @class Spyd.model.Component
 * @extends Ext.data.Model
 * Component model difinition.
 */
Ext.define('Spyd.model.Component', {
    extend: 'Ext.data.Model',
    require: 'Spyd.model.Accessory',

    fields: [{
        name: 'ComponentId',
        type: 'int'
    }, {
        name: 'ComponentName',
        type: 'string'
    }, {
        name: 'Amount',
        type: 'int'
    }, {
        name: 'AccessoryId',
        type: 'int'
    }],

    idProperty: 'ComponentId',

    proxy: {
        type: 'rest',
        url: '/spyd/accessories/{0}/components',
        extraParams: {
            adapter_id: 1
        },
        reader: {
            type: 'json',
            root: 'data'
        }
    }
});
