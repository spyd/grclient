/**
 * @class Spyd.model.Loan
 * @extends Ext.data.Model
 * Loan model difinition.
 */
Ext.define('Spyd.model.Loan', {
    extend: 'Ext.data.Model',

    fields: [{
        name: 'LoanId',
        type: 'int'
    }, {
        name: 'ResponsibleId',
        type: 'int'
    }, {
        name: 'EmployeeId',
        type: 'int'
    }, {
        name: 'AccessoryId',
        type: 'int'
    }, {
        name: 'LoanDate',
        type: 'date'
    }, {
        name: 'LoanHour',
        type: 'string'
    }, {
        name: 'ReturnDate',
        type: 'date'
    }, {
        name: 'ReturnHour',
        type: 'string'
    }, {
        name: 'Comments',
        type: 'string'
    }],

    idProperty: 'LoanId',

    proxy: {
        type: 'rest',
        url: '/spyd/loans',
        extraParams: {
            adapter_id: 1
        },
        reader: {
            type: 'json',
            root: 'data'
        }
    }
});
