/**
 * @class Spyd.model.Accessory
 * @extends Ext.data.Model
 * Accessory model difinition.
 */
Ext.define('Spyd.model.Accessory', {
    extend: 'Ext.data.Model',
    //require: 'Spyd.model.Component',

    fields: [{
        name: 'Id',
        type: 'int'
    }, {
        name: 'Name',
        type: 'string'
    }, {
        name: 'Description',
        type: 'string'
    }, {
        name: 'ImagePath',
        type: 'string'
    }, {
        name: 'AcquisitionDate',
        type: 'date'
    }, {
        name: 'EmployeeId',
        type: 'int'
    }, {
        name: 'StatusId',
        type: 'int'
    }, {
        name: 'Visible',
        type: 'bool'
    }],

    idProperty: 'Id',

    proxy: {
        type: 'rest',
        url: '/spyd/accessories',
        extraParams: {
            adapter_id: 1
        },
        reader: {
            type: 'json',
            root: 'data'
        }
    }
});
