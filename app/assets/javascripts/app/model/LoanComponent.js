/**
 * @class Spyd.model.LoanComponent
 * @extends Ext.data.Model
 * LoanComponent model difinition.
 */
Ext.define('Spyd.model.LoanComponent', {
    extend: 'Ext.data.Model',

    fields: [{
        name: 'LoanComponentId',
        type: 'int'
    }, {
        name: 'LoanId',
        type: 'int'
    }, {
        name: 'ComponentId',
        type: 'int'
    }, {
        name: 'AccessoryId',
        type: 'int'
    }, {
        name: 'LoanAmount',
        type: 'int'
    }, {
        name: 'IsLoaned',
        type: 'bool'
    }, {
        name: 'ReturnAmount',
        type: 'int'
    }, {
        name: 'ComponentName',
        type: 'string'
    }],

    idProperty: 'LoanComponentId',

    proxy: {
        type: 'rest',
        url: '/spyd/loans/{0}/loancomponents',
        extraParams: {
            adapter_id: 1
        },
        reader: {
            type: 'json',
            root: 'data'
        }
    }
});
