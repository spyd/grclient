/**
 * @class Spyd.model.Employee
 * @extends Ext.data.Model
 * Employee model difinition.
 */
Ext.define('Spyd.model.Employee', {
    extend: 'Ext.data.Model',
    //require: 'Spyd.model.Component',

    fields: [{
        name: 'EmployeeId',
        type: 'int'
    }, {
        name: 'Name',
        type: 'string'
    }, {
        name: 'LastName',
        type: 'string'
    }, {
        name: 'FullName',
        type: 'string'
    }, {
        name: 'Email',
        type: 'string'
    }, {
        name: 'Phone',
        type: 'string'
    }, {
        name: 'CellPhone',
        type: 'string'
    }, {
        name: 'RoleId',
        type: 'int'
    }],

    idProperty: 'EmployeeId',
/*
    hasMany: [{
        model: 'Spyd.model.Component',
        name: 'tabs'
    }],
*/
});
