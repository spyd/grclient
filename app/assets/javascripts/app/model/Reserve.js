/**
 * @class Spyd.model.Reserve
 * @extends Ext.data.Model
 * Reserve model difinition.
 */
Ext.define('Spyd.model.Reserve', {
    extend: 'Ext.data.Model',

    fields: [{
        name: 'ReserveId',
        type: 'int'
    }, {
        name: 'ResponsibleId',
        type: 'int'
    }, {
        name: 'EmployeeId',
        type: 'int'
    }, {
        name: 'AccessoryId',
        type: 'int'
    }, {
        name: 'ReserveDate',
        type: 'date'
    }, {
        name: 'ReserveHour',
        type: 'string'
    }, {
        name: 'InitialDate',
        type: 'date'
    }, {
        name: 'InitialHour',
        type: 'string'
    }, {
        name: 'FinalDate',
        type: 'date'
    }, {
        name: 'FinalHour',
        type: 'string'
    }, {
        name: 'Confirmed',
        type: 'bool'
    }, {
        name: 'Canceled',
        type: 'bool'
    }, {
        name: 'Note',
        type: 'string'
    }],

    idProperty: 'ReserveId',

    proxy: {
        type: 'rest',
        url: '/spyd/reserves',
        extraParams: {
            adapter_id: 1
        },
        reader: {
            type: 'json',
            root: 'data'
        }
    }
});
