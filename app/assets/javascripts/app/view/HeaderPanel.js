/**
 * @class Spyd.view.HeaderPanel
 */
Ext.define('Spyd.view.HeaderPanel', {
    extend: 'Ext.panel.Panel',
    
    requires: [
        'Spyd.view.MenuOption'
    ],

    alias: 'widget.headerpanel',
    cls: 'x-headerpanel',
    itemId: 'x-headerpanel',
    session: false,
    adminMode: false,
    height: 100,

    /**
     * Initialize component
     */
    initComponent: function () {
        var me = this;
        me.addEvents('toogleMenu');
        me.loadPanelComponents(me.session);
        me.callParent();
    },

    loadPanelComponents: function(isLoged) {
        var me = this,
            barMenuOption = Ext.create('Spyd.view.MenuOption', {adminMode:me.adminMode, region: 'center'}),
            menuUserOption = me.session === true ? {
                xtype: 'panel',
                itemId: 'p2-panel',
                layout: 'border',
                height: 30,
                bodyStyle: {"background-color":"#bcb0b0"},
                items: [{
                    xtype: 'toolbar',
                    itemId: 'user-info',
                    cls: 'user-info',
                    renderTo: document.body,
                    region: 'west',
                    bodyStyle: {"background-color":"#bcb0b0", "font-size": "15px"},
                    items: ['Bienvenido']
                }, barMenuOption]
            } : {};

        me.items = [{
            xtype: 'panel',
            itemId: 'p1-panel',
            layout: 'column',
            height: 70,
            bodyStyle: {"background-color":"silver", "color":"gray", "font-size": "30px"},
            items: [{
                xtype: 'image',
                itemId: 'imageId',
                name: 'accessoryImage',
                fieldLabel: 'Imagen ',
                src: '/assets/logo-image.gif',
                width: 100,
                height: 70
            }, {
                xtype: 'label',
                text: 'Sistema Integral de Prestamos y Devoluciones',
                margin: '30 0 0 10'
            }]
        }, menuUserOption];

    }
});