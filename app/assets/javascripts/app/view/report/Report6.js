/**
 * @class Spyd.view.report.Report6
 */
Ext.define('Spyd.view.report.Report6', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.spydreport6',
    
    requires: ['Spyd.store.Report'],

    itemId: 'report6-grid-view',
    cls: 'report6-grid-view',
    title: 'Reporte de accesorios reservados que no fueron usados',
    frame: true,
    
    initComponent: function() {
        var me = this;
        me.store = Ext.create('Spyd.store.Report', {
            storeId: 'Report6',
            generateModel: true,
            reportUrl: '/spyd/view/reports/6'
        });
        me.store.load({
            scope: me,
            callback: function (records, operation) {
                if (operation.response !== undefined) {
                    var fields, data = Ext.decode(operation.response.responseText);
                    if (data.data.Data.length > 0) {
                        fields = Object.getOwnPropertyNames(data.data.Data[0]);
                        me.store.model.setFields(fields);
                        me.store.loadRawData(data.data.Data, false);
                    }
                }
            }
        });

        Ext.apply(this, {
            columns: [{
                header: 'Nombre completo del Empleado',
                dataIndex: 'EmployeeName',
                flex: 1,
                editor: {
                    allowBlank: false
                }
            }, {
                header: 'Nombre del Accesorio',
                dataIndex: 'AccessoryName',
                flex: 1,
                editor: {
                    allowBlank: false
                }
            }, {
                header: 'Fecha y Hora de inicio',
                dataIndex: 'StarDate',
                flex: 1,
                editor: {
                    allowBlank: false
                }
            }, {
                header: 'Fecha y Hora de fin',
                dataIndex: 'EndDate',
                flex: 1,
                editor: {
                    allowBlank: false
                }
            }]
        });

        me.callParent();
    }
});