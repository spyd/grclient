/**
 * @class Spyd.view.report.ReportList
 */
Ext.define('Spyd.view.report.ReportList', {
    extend: 'Ext.tab.Panel',
    requires: ['Ext.ux.form.SearchField'],

    alias: 'widget.searchreportlist',
    cls: 'search-report-list',
    itemId: 'search-report-list',
    split: true,
    store: null,
    reportModel: null,

    /**
     * Initialize component
     */
    initComponent: function () {
        var me = this, store = Ext.create('Spyd.store.Report', {autoLoad:true}),
            resultTpl = Ext.create('Ext.XTemplate',
                '<tpl for=".">',
                    '<ul><li id="{ReportId}" class="item-report-list" value="{ReportId}"><span>{ReportName}</span></li></ul>',
                '</tpl>',{}
            );

        me.items = {
            overflowY: 'auto',
            xtype: 'dataview',
            cls: 'search-item',
            tpl: resultTpl,
            store: store,
            itemSelector: 'div.search-item',
            emptyText: '<div class="x-grid-empty">No Matching Threads</div>'
        };
        me.dockedItems = [{
            dock: 'top',
            xtype: 'toolbar',
            cls: 'search-toolbar',
            items: {
                cls: 'search-field',
                emptyText: 'Ingrese Reporte a buscar...',
                disabled: true,
                labelWidth: 30,
                xtype: 'searchfield',
                store: store
            }
        }];

        me.store = store;
        me.loadSelectedReportAction();
        me.callParent();
    },

    loadSelectedReportAction: function() {
        Ext.getBody().on('click', function(event, target) {
            var mainPnl = Ext.getCmp('spyd-main-panel'), reportCardPnl,
                searchListPnl = Ext.getCmp('search-report-list');
            if (searchListPnl.reportModel !== searchListPnl.store.findRecord('ReportId', target.value)) {
                searchListPnl.accessoryModel = searchListPnl.store.findRecord('ReportId', target.value);
                reportCardPnl = searchListPnl.up().down('#report-card-view');
                reportCardPnl.layout.setActiveItem(target.value - 1);
                Ext.each(target.parentElement.parentElement.childNodes, function(item, index) {
                    item.childNodes[0].classList.remove('selected-item');
                });
                target.classList.add('selected-item');
            }
        }, null, {delegate: '.item-report-list'});

        //Ext.getCmp('search-report-list').getEl().select('.item-report-list').elements[0].classList.add('selected-item');
    }
});