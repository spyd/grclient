/**
 * @class Spyd.view.report.Report2
 */
Ext.define('Spyd.view.report.Report2', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.spydreport2',
    
    requires: ['Spyd.store.Report'],

    itemId: 'report2-grid-view',
    cls: 'report2-grid-view',
    title: 'Los 10 empleados que más usan accesorios',
    frame: true,
    
    initComponent: function() {
        var me = this;
        me.store = Ext.create('Spyd.store.Report', {
            storeId: 'Report2',
            generateModel: true,
            reportUrl: '/spyd/view/reports/2'
        });
        me.store.load({
            scope: me,
            callback: function (records, operation) {
                if (operation.response !== undefined) {
                    var fields, data = Ext.decode(operation.response.responseText);
                    if (data.data.Data.length > 0) {
                        fields = Object.getOwnPropertyNames(data.data.Data[0]);
                        me.store.model.setFields(fields);
                        me.store.loadRawData(data.data.Data, false);
                    }
                }
            }
        });

        Ext.apply(this, {
            columns: [{
                header: 'Nombre completo del Empleado',
                dataIndex: 'EmployeeName',
                flex: 1,
                editor: {
                    allowBlank: false
                }
            }, {
                header: 'Numero de prestamos realizados',
                dataIndex: 'LoanAmount',
                flex: 1,
                editor: {
                    allowBlank: false
                }
            }]
        });

        me.callParent();
    }
});