/**
 * @class Spyd.view.report.Report3
 */
Ext.define('Spyd.view.report.Report3', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.spydreport3',
    
    requires: ['Spyd.store.Report'],

    itemId: 'report3-grid-view',
    cls: 'report3-grid-view',
    title: 'Lista de los empleados que no devolvieron los accesorios completos',
    frame: true,
    
    initComponent: function() {
        var me = this;
        me.store = Ext.create('Spyd.store.Report', {
            storeId: 'Report3',
            generateModel: true,
            reportUrl: '/spyd/view/reports/3'
        });
        me.store.load({
            scope: me,
            callback: function (records, operation) {
                if (operation.response !== undefined) {
                    var fields, data = Ext.decode(operation.response.responseText);
                    if (data.data.Data.length > 0) {
                        fields = Object.getOwnPropertyNames(data.data.Data[0]);
                        me.store.model.setFields(fields);
                        me.store.loadRawData(data.data.Data, false);
                    }
                }
            }
        });

        Ext.apply(this, {
            columns: [{
                header: 'Nombre completo del Empleado',
                dataIndex: 'EmployeeName',
                flex: 1,
                editor: {
                    allowBlank: false
                }
            }, {
                header: 'Nombre del Accesorio',
                dataIndex: 'AccessoryName',
                flex: 1,
                editor: {
                    allowBlank: false
                }
            }, {
                header: 'Nombre del Componente',
                dataIndex: 'ComponentName',
                flex: 1,
                editor: {
                    allowBlank: false
                }
            }, {
                header: 'Cantidad Prestada',
                dataIndex: 'LoanAmount',
                flex: 1,
                editor: {
                    allowBlank: false
                }
            }, {
                header: 'Cantidad Devuelta',
                dataIndex: 'ReturnAmount',
                flex: 1,
                editor: {
                    allowBlank: false
                }
            }, {
                header: 'Observacion',
                dataIndex: 'Comments',
                flex: 1,
                editor: {
                    allowBlank: false
                }
            }]
        });

        me.callParent();
    }
});