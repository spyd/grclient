/**
 * @class Spyd.view.report.ReportForm
 */
Ext.define('Spyd.view.report.ReportForm', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.spydreportview',
    requires: [
        'Spyd.view.report.Report1',
        'Spyd.view.report.Report2',
        'Spyd.view.report.Report3',
        'Spyd.view.report.Report4',
        'Spyd.view.report.Report5',
        'Spyd.view.report.Report6',
        'Spyd.view.report.Report7',
        'Spyd.view.report.ReportList'
    ],
    cls: 'spyd-report-view',
    itemId: 'spyd-report-view',
    layout: 'border',
    height: 550,

    /**
     * Initialize component
     */
    initComponent: function () {
        var me = this;
        me.items = [{
            xtype: 'searchreportlist',
            id: 'search-report-list',
            title: 'Lista de Reportes',
            region: 'west',
            width: 350
        }, {
            xtype: 'panel',
            itemId: 'report-card-view',
            region: 'center',
            layout: 'card',
            items: [
                {xtype: 'spydreport1'}, {xtype: 'spydreport2'}, {xtype: 'spydreport3'}, {xtype: 'spydreport4'},
                {xtype: 'spydreport5'}, {xtype: 'spydreport6'}, {xtype: 'spydreport7'}
            ]
        }];
        me.callParent();
    }
});
