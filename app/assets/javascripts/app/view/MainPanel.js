/**
 * @class Spyd.view.MainPanel
 */
Ext.define('Spyd.view.MainPanel', {
    requires: [
        'Spyd.view.MainControl',
        'Spyd.view.reserve.ReserveForm',
        'Spyd.view.report.ReportForm'
    ],

    extend: 'Ext.panel.Panel',
    alias: 'widget.mainpanel',

    title: '',
    cls: 'main-panel',
    id: 'spyd-main-panel',
    itemId: 'main-panel',
    layout: 'card',
    accountStore: Ext.create('Spyd.store.Account'),
    userIdSession: 0,
    userNameSession: '',
    session: false,
    adminMode: false,

    /**
     * Initialize component
     */
    initComponent: function () {
        var me = this;
        me.addEvents('startUserSession');
        me.loadPanelComponents(me.session);
        me.callParent();
    },

    loadPanelComponents: function(isLoged){
        var me = this,
            loginPanel = {
                xtype: 'panel',
                itemId: 'login-view',
                layout:'ux.center',
                items: [{
                    title: 'Iniciar Sesión de Usuario',
                    itemId: 'login-panel',
                    bodyPadding: '15 10',
                    bodyStyle: {"background-color":"#CCC"},
                    widthRatio: 0.40,
                    items: [{
                        xtype: 'textfield',
                        fieldLabel: 'Usuario',
                        emptyText: 'Ingrese Cuenta del Empleado',
                        name: 'username',
                        itemId: 'username',
                        size: 50,
                        allowblank: false
                    }, {
                        xtype: 'textfield',
                        fieldLabel: 'Contraseña',
                        emptyText: 'Ingrese Contraseña del Empleado',
                        name: 'password',
                        itemId: 'password',
                        allowblank: false,
                        size: 50,
                        inputType: 'password'
                    }, {
                        xtype: 'panel',
                        itemId: 'login-pnl-btn',
                        bodyStyle: {"background-color":"#CCC"},
                        margin: '15 0',
                        layout:'ux.center',
                        items: {
                            xtype: 'button',
                            text: 'Iniciar Sesión',
                            itemId: 'login-btn',
                            action: 'login-session',
                            widthRatio: 0.30,
                            handler: function () {
                                me.fireEvent('startUserSession', me);
                            }
                        }
                    }]
                }],
            };
        me.items = [{
            xtype: 'panel',
            itemId: 'spyd-login-panel',
            flex: 1,
            items: [Ext.create('Spyd.view.HeaderPanel'), loginPanel]
        }, {
            xtype: 'panel',
            itemId: 'spyd-employee-view',
            flex: 1,
            items: [Ext.create('Spyd.view.HeaderPanel', {session: true}), {xtype: 'reservemanager'}]
        }, {
            xtype: 'panel',
            itemId: 'spyd-admin-view',
            flex: 1,
            items: [
                Ext.create('Spyd.view.HeaderPanel', {session: true, adminMode: true}), {
                    xtype: 'panel',
                    itemId: 'spyd-manage-card',
                    layout: 'card',
                    items: [Ext.create('Spyd.view.MainControl'), Ext.create('Spyd.view.reserve.ReserveAdmin'), Ext.create('Spyd.view.report.ReportForm')]
                }
            ]
        }];
    }
});