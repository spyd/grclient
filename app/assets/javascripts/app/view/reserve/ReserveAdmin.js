/**
 * @class Spyd.view.reserve.ReserveAdmin
 */
Ext.define('Spyd.view.reserve.ReserveAdmin', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.managereserveslist',
    requires: [
        'Spyd.view.reserve.ReserveCalendar',
        'Spyd.view.reserve.ReserveSearchList'
    ],
    cls: 'manage-reserve-list',
    itemId: 'manage-reserve-list',
    layout: 'border',
    //width: 800,
    height: 550,

    /**
     * Initialize component
     */
    initComponent: function () {
        var me = this,
            listSearch = Ext.create('Spyd.view.reserve.ReserveSearchList', {
                id: 'admin-search-accessory-reserve',
                title: 'Lista de Accesorios',
                defaultOption: true,
                collapsed: false,
                region: 'west',
                disabled: false
            }),
            myReserves = Ext.create('Spyd.view.reserve.ReserveCalendar', {
                title: 'Calendario de Reservas Programadas',
                region: 'center',
                viewConfig: {
                    enableEditDetails: false,
                    enableContextMenus: true
                }
            });

        me.items = [listSearch, myReserves];
        me.callParent();
    }
});
