/**
 * @class Spyd.view.reserve.ReserveForm
 */
Ext.define('Spyd.view.reserve.ReserveForm', {
    extend: 'Ext.tab.Panel',
    requires: [
        'Spyd.view.reserve.ReserveAccessory',
        'Spyd.view.reserve.ReserveEmployee'
    ],

    alias: 'widget.reservemanager',
    cls: 'reserve-form',
    id: 'reserve-accessory-form',
    itemId: 'reserve-form',
    activeTab: 0,

    /**
     * Initialize component
     */
    initComponent: function () {
        var me = this;
        me.items = [{
            title: 'Reservas',
            xtype: 'reserveaccessory'
        }, {
            title: 'Mis reservas',
            xtype: 'myreserveslist'
        }];

        me.callParent();
    }
});