/**
 * @class Spyd.view.reserve.ReserveAccessory
 */
Ext.define('Spyd.view.reserve.ReserveAccessory', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.reserveaccessory',
    requires: [
        'Spyd.view.reserve.ReserveCalendar',
        'Spyd.view.reserve.ReserveSearchList'
    ],
    cls: 'reserve-accessory',
    itemId: 'reserve-accessory',
    layout: 'border',
    //width: 800,
    height: 550,

    /**
     * Initialize component
     */
    initComponent: function () {
        var me = this;
        me.items = [{
            xtype: 'searchaccessorylist',
            id: 'search-accessory-list-reserve',
            title: 'Lista de Accesorios',
            region: 'west',
            //width: 270
        }, {
            xtype: 'spydreservecalendar',
            title: 'Calendario de Reservas',
            region: 'center'
        }];
        me.callParent();
    }
});
