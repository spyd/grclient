/**
 * @class Spyd.view.reserve.ReserveCalendar
 */
Ext.define('Spyd.view.reserve.ReserveCalendar', {
    extend: 'Extensible.calendar.CalendarPanel',
    alias: ['widget.spydreservecalendar'],
    requires: [
        //'Extensible.calendar.data.MemoryEventStore',
        //'Ext.ux.form.SearchField'
    ],

    cls: 'reserve-calendar',
    itemId: 'reserve-calendar',
    activeItem: 1,
    // this is a good idea since we are in a TabPanel and we don't want
    // the user switching tabs on us while we are editing an event:
    editModal: true,
    // These show by default, turn them off
    showDayView: false,
    showMonthView: false,
    
    // Defaults to 3 days. You could also set the dayCount config
    // inside multiDayViewCfg to change that.
    showMultiDayView: false,
    showMultiWeekView: false,

    showNavBar: false,
    //showNavNextPrev: false,
    enableEditDetails: false,
    
    weekText: 'Semana Reservas',

    todayText: 'Hoy',

    viewConfig: {
        //enableFx: false,
        //enableAddFx: false,
        //enableUpdateFx: false,
        //enableRemoveFx: false,
        //enableDD: false,
        trackMouseOver: false,
        enableEditDetails: false,
        enableContextMenus: false
    },

    weekViewCfg: {
        // This view will only show Mon-Fri.
        dayCount: 5,
        // Always start the view on Monday
        startDay: 1,
        startDayIsStatic: true,
        showHourSeparator: true,
        viewStartHour: 8,
        viewEndHour: 20,
        scrollStartHour: 8
    },

    /*
    listeners: {
        dayclick: function (fileBtn) {
            console.log('hello world');
        }
    },*/
    eventScheduled: {},
    reserveStore:  Ext.create('Spyd.store.Reserve'),

    /**
     * Initialize component
     */
    initComponent: function () {
        var me = this; 
        me.eventStore = Ext.create('Extensible.calendar.data.MemoryEventStore', {
            data: me.eventScheduled
        }),

        me.callParent();
    },

    loadReservesCalendar: function(accessoryModel) {
        var me = this, title, span, itemClass, record,
            employeeId = me.up('#main-panel').userIdSession;
        me.reserveStore.load({
            scope: me,
            callback: function(reserveStore) {
                me.store.removeAll();
                reserveStore.forEach(function(reserve) {
                    if (accessoryModel.get('Id') === reserve.get('AccessoryId')) {
                        itemClass = employeeId === reserve.get('EmployeeId') ? "own-reserve-item" : "reserve-item";
                        span = Ext.String.format('<span style="display:none" class="{0}">{1}</span>', itemClass, reserve.get('InitialDate'));
                        title = Ext.String.format('<br> {0} <br/> Nota: {1} {2}', accessoryModel.get('Name'), reserve.get('Note'), span);
                        if (reserve.get('Confirmed') === true) {
                            title = Ext.String.format(' (Reserva Confirmada) {0}', title);
                        }
                        if (reserve.get('Canceled') === false) {
                            var record = new Extensible.calendar.data.EventModel({
                                CalendarId: reserve.get('ReserveId'),
                                StartDate: reserve.get('InitialDate'),
                                EndDate: reserve.get('FinalDate'),
                                Title: title,
                                Notes: reserve.get('Note')
                            });
                            me.onEventAdd(me, record);
                        }
                    }
                });
                me.customizeCalendarDays(false);
            }
        });
    },

    loadMyReservesCalendar: function(accessoryStore, employeeId) {
        var me = this, accessoryModel, title, span, itemClass, record;
        me.reserveStore.load({
            scope: me,
            callback: function(reserveStore) {
                me.store.removeAll();
                reserveStore.forEach(function(reserve) {
                    if (employeeId === reserve.get('EmployeeId')) {
                        accessoryModel = accessoryStore.findRecord('Id', reserve.get('AccessoryId'));
                        itemClass = employeeId === reserve.get('EmployeeId') ? "own-reserve-item" : "reserve-item";
                        span = Ext.String.format('<span style="display:none" class="{0}">{1}</span>', itemClass, reserve.get('InitialDate'));
                        title = Ext.String.format('<br> {0} <br/> Nota: {1} {2}', accessoryModel.get('Name'), reserve.get('Note'), span);
                        if (reserve.get('Confirmed') === true) {
                            title = Ext.String.format(' (Reserva Confirmada) {0}', title);
                        }
                        if (reserve.get('Canceled') === false) {
                            var record = new Extensible.calendar.data.EventModel({
                                CalendarId: reserve.get('ReserveId'),
                                StartDate: reserve.get('InitialDate'),
                                EndDate: reserve.get('FinalDate'),
                                Title: title,
                                Notes: reserve.get('Note'),
                                ColorId: 3,
                                Reserve: reserve,
                                EmployeeId: employeeId
                            });
                            me.onEventAdd(me, record);
                        }
                    }
                });
                //me.customizeCalendarDays(false);
            }
        });
    },

    loadListReservesCalendar: function(accessoryStore, employeeId) {
        var me = this, accessoryModel, title, record;
        me.reserveStore.load({
            scope: me,
            callback: function(reserveStore) {
                me.store.removeAll();
                reserveStore.forEach(function(reserve) {
                    accessoryModel = accessoryStore.findRecord('Id', reserve.get('AccessoryId'));
                    title = Ext.String.format('<br> {0} <br/> Nota: {1}', accessoryModel.get('Name'), reserve.get('Note'));
                    if (reserve.get('Confirmed') === true) {
                        title = Ext.String.format(' (Reserva Confirmada) {0}', title);
                    }
                    if (reserve.get('Canceled') === false) {
                        var record = new Extensible.calendar.data.EventModel({
                            CalendarId: reserve.get('ReserveId'),
                            StartDate: reserve.get('InitialDate'),
                            EndDate: reserve.get('FinalDate'),
                            Title: title,
                            Notes: reserve.get('Note'),
                            Reserve: reserve,
                            EmployeeId: employeeId
                        });
                        me.onEventAdd(me, record);
                    }
                });

                var searchPnlItems = Ext.getCmp('admin-search-accessory-reserve').getEl().select('.item-accessory');
                Ext.each(searchPnlItems.elements, function(item, index) {
                    item.classList.remove('selected-item');
                });
                searchPnlItems.elements[0].classList.add('selected-item');
            }
        });
    },

    loadAdminReservesCalendar: function(accessoryModel) {
        var me = this, title, span, record,
            employeeId = me.up('#main-panel').userIdSession;
        me.reserveStore.load({
            scope: me,
            callback: function(reserveStore) {
                me.store.removeAll();
                reserveStore.forEach(function(reserve) {
                    if (accessoryModel.get('Id') === reserve.get('AccessoryId')) {
                        span = Ext.String.format('<span style="display:none" class="reserve-item">{0}</span>', reserve.get('InitialDate'));
                        title = Ext.String.format('<br> {0} <br/> Nota: {1} {2}', accessoryModel.get('Name'), reserve.get('Note'), span);
                        if (reserve.get('Confirmed') === true) {
                            title = Ext.String.format(' (Reserva Confirmada) {0}', title);
                        }
                        if (reserve.get('Canceled') === false) {
                            var record = new Extensible.calendar.data.EventModel({
                                CalendarId: reserve.get('ReserveId'),
                                StartDate: reserve.get('InitialDate'),
                                EndDate: reserve.get('FinalDate'),
                                Title: title,
                                Notes: reserve.get('Note'),
                                Reserve: reserve,
                                EmployeeId: employeeId
                            });
                            me.onEventAdd(me, record);
                        }
                    }
                });
                me.customizeCalendarDays(false);
            }
        });
    },

    customizeCalendarDays: function(defaultColor) {
        Ext.each($('.ext-cal-day-col'), function(dayColumn, index) {
            var currentDate = new Date();
            if (currentDate.getDay() === index + 1) {
                dayColumn.classList.add('today-column-day');
                dayColumn.style.backgroundColor = "#FFF4BF";
            }
        });

        Ext.each($('.own-reserve-item'), function(item, index) {
            var parentPnl = item.parentElement.parentElement;
            parentPnl.parentElement.style.width = "100%"
        });

        Ext.each($('.reserve-item'), function(item, index) {
            var parentPnl = item.parentElement.parentElement;
            parentPnl.style.backgroundColor = defaultColor === true ? "#7CB22A" : "#668CB3";
            parentPnl.parentElement.style.width = "100%"
        });
    }
});