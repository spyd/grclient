/**
 * @class Spyd.view.reserve.ReserveEmployee
 */
Ext.define('Spyd.view.reserve.ReserveEmployee', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.myreserveslist',
    requires: [
        'Spyd.view.reserve.ReserveCalendar',
        'Spyd.view.reserve.ReserveSearchList'
    ],
    cls: 'my-reserve-list',
    itemId: 'my-reserve-list',
    layout: 'border',
    //width: 800,
    height: 550,

    /**
     * Initialize component
     */
    initComponent: function () {
        var me = this,
            listSearch = Ext.create('Spyd.view.reserve.ReserveSearchList', {
                title: 'Lista de Accesorios',
                region: 'west',
                collapsed: true,
                disabled: true
            }),
            myReserves = Ext.create('Spyd.view.reserve.ReserveCalendar', {
                title: 'Calendario de Reservas Programadas',
                region: 'center',
                viewConfig: {
                    enableEditDetails: false,
                    enableContextMenus: true
                }
            });

        me.items = [listSearch, myReserves];
        me.callParent();
    }
});
