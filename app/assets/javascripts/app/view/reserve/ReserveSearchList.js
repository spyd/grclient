/**
 * @class Spyd.view.reserve.ReserveSearchList
 */
Ext.define('Spyd.view.reserve.ReserveSearchList', {
    extend: 'Ext.tab.Panel',
    requires: ['Ext.ux.form.SearchField'],

    alias: 'widget.searchaccessorylist',
    cls: 'search-accessory-list',
    itemId: 'search-accessory-list',
    split: true,
    store: null,
    accessoryModel: null,
    defaultOption: false,

    /**
     * Initialize component
     */
    initComponent: function () {
        var me = this,
            store = Ext.create('Spyd.store.Accessory', {autoLoad:true}),
            resultTpl = Ext.create('Ext.XTemplate',
                '<tpl for=".">',
                    '<ul><li id="{Id}" class="item-accessory" value="{Id}"><span>{Name}</span></li></ul>',
                '</tpl>',{}
            );
        if (me.defaultOption === true) {
            resultTpl = Ext.create('Ext.XTemplate',
                '<ul><li id="0" class="item-accessory" value="0"><span>Mostrar Todos</span></li></ul>',
                '<tpl for=".">',
                    '<ul><li id="{Id}" class="item-accessory" value="{Id}"><span>{Name}</span></li></ul>',
                '</tpl>',{}
            );
        }

        me.items = {
            overflowY: 'auto',
            xtype: 'dataview',
            cls: 'search-item',
            tpl: resultTpl,
            store: store,
            itemSelector: 'div.search-item',
            emptyText: '<div class="x-grid-empty">No Matching Threads</div>'
        };
        me.dockedItems = [{
            dock: 'top',
            xtype: 'toolbar',
            cls: 'search-toolbar',
            items: {
                cls: 'search-field',
                emptyText: 'Ingrese Accesorio a buscar...',
                disabled: true,
                labelWidth: 30,
                xtype: 'searchfield',
                store: store
            }
        }, {
            dock: 'bottom',
            xtype: 'pagingtoolbar',
            store: store,
            pageSize: 25,
            displayInfo: true,
            displayMsg: 'Topics {0} - {1} of {2}',
            emptyMsg: 'No topics to display'
        }];

        me.store = store;
        me.loadSelectedAccesoryAction();
        me.callParent();
    },

    loadSelectedAccesoryAction: function() {
        Ext.getBody().on('click', function(event, target) {
            var mainPnl = Ext.getCmp('spyd-main-panel'),
                searchListPnl, calendarPnl;
            if (mainPnl.adminMode === true) {
                searchListPnl = Ext.getCmp('admin-search-accessory-reserve');
            } else {
                searchListPnl = Ext.getCmp('search-accessory-list-reserve');
            }
            if (searchListPnl.accessoryModel !== searchListPnl.store.findRecord('Id', target.value)) {
                searchListPnl.accessoryModel = searchListPnl.store.findRecord('Id', target.value);
                calendarPnl = searchListPnl.up().down('#reserve-calendar');
                if (mainPnl.adminMode === true) {
                    if (searchListPnl.accessoryModel === null && target.value === 0) {
                        calendarPnl.loadListReservesCalendar(searchListPnl.store, mainPnl.userIdSession);
                    } else {
                        calendarPnl.loadAdminReservesCalendar(searchListPnl.accessoryModel);
                    }
                } else {
                    calendarPnl.loadReservesCalendar(searchListPnl.accessoryModel);
                }
                Ext.each(target.parentElement.parentElement.childNodes, function(item, index) {
                    item.childNodes[0].classList.remove('selected-item');
                });
                target.classList.add('selected-item');
            }
        }, null, {delegate: '.item-accessory'});
    }
});