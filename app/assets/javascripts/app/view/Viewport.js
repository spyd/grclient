/**
 * @class Spyd.view.Viewport
 */
Ext.define('Spyd.view.Viewport', {
    extend: 'Ext.container.Viewport',

    requires: [
        'Spyd.view.HeaderPanel',
        'Spyd.view.MainPanel',
        'Spyd.view.MainControl',
        'Spyd.view.MenuOption',
        'Spyd.view.reserve.ReserveForm',
        'Spyd.view.report.ReportForm',
        'Spyd.util.Util'
    ],

    layout: {
        align: 'stretch',
        type: 'vbox'
    },

    /**
     * Initialize component
     */
    initComponent: function () {
        var me = this,
            mainPnl = Ext.create('Spyd.view.MainPanel');
        me.items = [mainPnl];

        me.callParent(arguments);
    }
}); 