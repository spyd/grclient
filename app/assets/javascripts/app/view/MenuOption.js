/**
 * @class Spyd.view.MenuOption
 */
Ext.define('Spyd.view.MenuOption', {
    extend: 'Ext.toolbar.Toolbar',
    alias: 'widget.barmenuoption',
    requires: ['Ext.ux.form.SearchField'],

    itemId: 'bar-menu',
    cls: 'bar-menu',
    renderTo: document.body,
    adminMode: false,

    initComponent : function () {
        this.initialize();
        this.callParent(arguments);
    },

    initialize : function () {
        var me = this;

        var menuOptions = {
            xtype: 'button',
            text: 'Opciones',
            menu: {
                xtype: 'menu',
                renderTo: Ext.getBody(),
                itemId: 'options-menu',
                cls: 'options-menu',
                text: 'Opciones',
                items: [{
                    text: 'Lista de Accesorios',
                    itemId: 'accessory-view-btn',
                    action: 'list-accessory-view',
                    scope: me
                }, {
                    text: 'Reservas de Accesorios',
                    itemId: 'reserves-view-btn',
                    action: 'reserve-accessory',
                    scope: me
                }, {
                    text: 'Reportes',
                    itemId: 'reports-view-btn',
                    action: 'reports-accessory',
                    scope: me
                }]
            }
        };

        var menuAccessory = {
            xtype: 'button',
            text: 'Accesorios',
            menu: {
                xtype: 'menu',
                renderTo: Ext.getBody(),
                itemId: 'accessory-menu',
                cls: 'accessory-menu',
                text: 'Accesorios',
                items: [{
                    text: 'Adicionar Accesorio',
                    itemId: 'add-accessory-btn',
                    action: 'create-accessory',
                    scope: me
                }, {
                    text: 'Editar Accesorio',
                    action: 'edit-accessory',
                    scope: me
                }, {
                    text: 'Eliminar Accesorio',
                    action: 'delete-accessory',
                    scope: me
                }]
            }
        };
        var logoutOption = {
            xtype: 'button',
            text: 'Cerrar Sesión',
            itemId: 'logout-btn',
            action: 'logout-session',
            scope: me
        };

        me.items = me.adminMode === true ? [
            '->',
            menuAccessory,
            menuOptions,
            {
                xtype    : 'searchfield',
                name     : 'filteraccessoryview',
                emptyText: 'Ingrese Accesorio a buscar',
                size: 200,
                disabled: true,
                store: Ext.create('Spyd.store.AccessoryView')
            },
            { xtype: 'tbspacer', width: 10 },
            logoutOption
        ] : ['->', logoutOption];
    }
});
