/**
 * @class Spyd.view.accessory.AccessoryElementGrid
 */
Ext.define('Spyd.view.accessory.AccessoryElementGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.componentsgrid',
    
    requires: [
        'Spyd.store.Component',
        'Spyd.model.Component'
    ],

    xtype: 'cell-editing',
    title: 'Lista de Componentes',
    frame: true,
    
    initComponent: function() {
        this.cellEditing = new Ext.grid.plugin.CellEditing({
            clicksToEdit: 1
        });
        
        Ext.apply(this, {
            plugins: [this.cellEditing],
            columns: [{
                header: 'Nombre Componente',
                dataIndex: 'ComponentName',
                flex: 1,
                editor: {
                    allowBlank: false,
                    //regex: new RegExp("^[A-Za-z0-9 ]+$"),
                    //regexText: 'No permite caracteres Especiales',

                }
            }, {
                header: 'Cantidad',
                dataIndex: 'Amount',
                width: 100,
                align: 'right',
                editor: {
                    xtype: 'numberfield',
                    allowBlank: false,
                    minValue: 1,
                    maxValue: 100
                }
            }, {
                xtype: 'actioncolumn',
                width: 30,
                sortable: false,
                menuDisabled: true,
                items: [{
                    icon: '/assets/icons/close_remove_cross.png',
                    tooltip: 'Eliminar Componente',
                    scope: this,
                    handler: this.onRemoveClick
                }]
            }],
            tools: [{
                type: 'gear',
                scope: this,
                tooltip: 'Editar Componente',
                handler: function () {
                    console.log('component edited');
                }
            }, {
                type: 'plus',
                scope: this,
                tooltip: 'Añadir Componente',
                handler: this.onAddClick
            }]
        });
        
        this.callParent();
        this.on('afterlayout', this.loadStore, this, {
            delay: 1,
            single: true
        });
    },
    
    loadStore: function() {
        if (this.store.isLoading() === false) {
            this.getStore().load();
        }
    },
    
    onAddClick: function(){
        var newComponent = Ext.create('Spyd.model.Component', {});
        this.getStore().insert(0, newComponent);
        this.cellEditing.startEditByPosition({
            row: 0, 
            column: 0
        });
    },
    
    onRemoveClick: function(grid, rowIndex){
        this.getStore().removeAt(rowIndex);
    }
});