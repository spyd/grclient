/**
 * @class Spyd.view.accessory.AccessoryForm
 */
Ext.define('Spyd.view.accessory.AccessoryForm', {
    requires: [
        'Spyd.view.accessory.AccessoryElementGrid',
        'Spyd.store.Employee',
        'Spyd.store.Accessory',
        'Spyd.store.Component'
    ],

    extend: 'Ext.window.Window',
    alias: 'widget.accessorymanager',
    region: 'center',
    layout: 'fit',
    title: 'Registro de Accesorios',
    overflow: true,
    overflowY: 'auto',
    height: 500,
    width: 600,
    cls: 'accessory-manager',
    itemId: 'accessory-manager',
    resizable: true,
    parentGridPanel: null,
    editMode: false,
    store: Ext.create('Spyd.store.Accessory'),
    componentStore: null,
    accessoryModel: null,
    defaults: {
        anchor: '100%',
        border: false
    },
 
    /**
     * Button docked bar
     */
    fbar: [{
        text: 'Registrar',
        width: 75,
        handler: function (btn) {
            var w = btn.up('accessorymanager');
            w.handleSaveAccessory();
        }
    }, {
        text: 'Cancelar',
        width: 75,
        handler: function (btn) {
            var w = btn.up('accessorymanager');
            w.handleCancel();
        }
    }, {
        text: 'Limpiar',
        width: 75,
        handler: function (btn) {
            var w = btn.up('accessorymanager');
            w.handleClean();
        }
    }],

    /**
     * Initialize component
     */
    initComponent: function () {
        var me = this;

        me.items = [{
            xtype: 'panel',
            itemId: 'accessory-form',
            cls: 'accessory-form',
            title: 'Nuevos Accesorios',
            collapsible: false,
            layout: {
                type: 'border'
            },
            items: [{
                xtype: 'panel',
                region:'center',
                margins: '5 0 0 0',
                itemId: 'pnl-accessory',
                cls: 'pnl-accessory',
                items: [{
                    xtype: 'textfield',
                    value: null,
                    name: 'name',
                    itemId: 'nameId',
                    fieldLabel: 'Nombre',
                    emptyText: 'Nombre del Accesorio',
                    regex: new RegExp("^[A-Za-z0-9 ]+$"),
                    regexText: 'No permite caracteres Especiales',
                    blankText : 'Nombre del Accesorio es requerido.',
                    margin: '5 0 10 10',
                    size: 30,
                    allowBlank: false,
                    maxLenght: 99,
                    maxLenghtText: 'el tamaño debe ser menor a 100',
                    validator: function(value){
                        if(value.length >= 100) {
                            return 'El tamaño debe ser menor a 100';
                        } else {
                            return true;
                        }
                    }
                }, {
                    xtype: 'textareafield',
                    name: 'description',
                    itemId: 'descriptionId',
                    fieldLabel: 'Descripción',
                    emptyText: 'Descripción del Accesorio',
                    margin: '0 0 10 10',
                    width:320,
                    regex: new RegExp("^[A-Za-z0-9 ]+$"),
                    regexText: 'No permite caracteres Especiales',
                    allowBlank: true,
                    maxLenght: 499,
                    maxLenghtText: 'el tamaño debe ser menor a 500',
                    validator: function(value){
                        if(value.length >= 500) {
                            return 'El tamaño debe ser menor a 500';
                        } else {
                            return true;
                        }
                    }
                }, {
                    xtype: 'combo',
                    name: 'owner',
                    store: Ext.create('Spyd.store.Employee').load(),
                    fieldLabel: 'Responsable',
                    itemId: 'ownerId',
                    valueField: 'EmployeeId',
                    displayField: 'FullName',
                    emptyText: 'Seleccione un Responsable',
                    queryMode: 'local',
                    margin: '0 0 10 10',
                    size: 30,
                    editable: false,
                    listeners: {
                        render: function (cmb) {
                            cmb.setValue(Ext.getCmp('spyd-main-panel').userIdSession);
                        }
                    }
                }, {
                    xtype: 'datefield',
                    name: 'enterdate',
                    value: new Date(),
                    itemId: 'enterdateId',
                    fieldLabel: 'Fecha de adquisición',
                    margin: '0 0 10 10',
                    width: 320,
                    blankText : "Fecha de registro debe ser seleccionada.",
                    editable: false,
                    handler: function(picker, date) {
                        Ext.Msg.alert('Date Selected', Ext.Date.format(date, 'M j, Y'));
                    }
                }]
            }, {
                xtype: 'panel',
                region:'east',
                itemId: 'pnl-image',
                cls: 'pnl-image',
                margins: '5 0 0 0',
                height: 200,
                width: 250,
                items: [{
                    xtype: 'filefield',
                    cls : 'preview-image-btn',
                    itemId: 'preview-image-btn',
                    name: 'previewImage',
                    split: false,
                    emptyText: 'Subir Imagen',
                    margin: '5 0 10 0',
                    buttonText: 'Seleccione Imagen',
                    resourceImage: null,
                    width: 240,
                    allowBlank: true,
                    listeners: {
                        render: function (fileBtn) {
                            var parent = fileBtn.getEl().parent(),
                                srcImage = fileBtn.resourceImage === null ? '/assets/accessories/sample-accessory.jpg' : fileBtn.resourceImage,
                                outerDiv = Ext.DomHelper.append(parent, {
                                    tag: 'div', 
                                    cls: 'tab-logo-div',
                                    margin: 10,
                                    height: 200
                                }, true),
                                img = Ext.DomHelper.append(outerDiv, {
                                    tag: 'img',
                                    cls: "tab-logo-image",
                                    width: 240,
                                    height: 150,
                                    src: srcImage
                                }, true);

                            fileBtn.getEl().insertBefore(outerDiv);
                            fileBtn.image = img;
                        },
                        change: function (fileBtn) {
                            var imageFile = '/assets/accessories/' + fileBtn.getValue().split('\\')[2];
                            fileBtn.image.dom.setAttribute('src', imageFile);
                        }
                    }
                }]
            },{
                xtype: 'componentsgrid',
                region: 'south',
                itemId: 'pnl-grid-items',
                cls: 'pnl-grid-items',
                store: me.componentStore !== null ? me.componentStore : Ext.create('Spyd.store.Component', {autoLoad: false}),
                height: 200,
                width: 500,
                cmargins: '5 0 0 0'
            }]
        }];

        if (me.editMode === false) {
            me.initAccessoryRegister();
            me.addEvents('createAccessory');
        } else {
            me.initAccessoryEditor();
            me.addEvents('editAccessory');
        }
        me.callParent();
    },

    initAccessoryRegister: function () {
    },

    initAccessoryEditor: function () {
        var me = this;
        me.fbar = [{
            text: 'Editar',
            width: 75,
            handler: function (btn) {
                var w = btn.up('accessorymanager');
                w.handleEditAccessory();
            }
        }, {
            text: 'Cancelar',
            width: 75,
            handler: function (btn) {
                var w = btn.up('accessorymanager');
                w.handleCancel();
            }
        }];
    },

    loadSelectedAccessory: function(selectedAccessory) {
        var me = this,
            accessoryForm = me.down('#pnl-accessory'),
            accessoryImage = me.down('#preview-image-btn'),
            gridComponents = me.down('#pnl-grid-items'),
            imageFile = selectedAccessory.data.ImagePath === '' || selectedAccessory.data.ImagePath === null ?
                        '/assets/accessories/sample-accessory.jpg' :
                        '/assets/accessories/' + selectedAccessory.data.ImagePath;

        accessoryForm.items.get('nameId').setValue(selectedAccessory.data.Name);
        accessoryForm.items.get('descriptionId').setValue(selectedAccessory.data.Description);
        accessoryForm.items.get('ownerId').setValue(selectedAccessory.data.EmployeeId);
        accessoryForm.items.get('enterdateId').setValue(selectedAccessory.data.AcquisitionDate);
        accessoryImage.resourceImage = imageFile;
    },

    /**
     * Handle to save new accessory.
     * @private
     */
    handleSaveAccessory: function () {
        var me = this,
            gridComponents = me.down('#pnl-grid-items'),
            validation = me.validateDataAccessory(me),
            validationGrid = Spyd.util.Util.validateGrid(gridComponents);
        if (validation === true && validationGrid === true) {
            me.fireEvent('createAccessory', me);
            this.close();
            Ext.Msg.alert('Información', 'El Accesorio fue registrado con Exito');
        }
    },

    /**
     * Handle to save new accessory.
     * @private
     */
    handleEditAccessory: function () {
        var me = this,
            gridComponents = me.down('#pnl-grid-items'),
            validation = me.validateDataAccessory(me);
        if (validation === true) {
            me.fireEvent('editAccessory', me);
            //gridComponents.getStore().removeAll();
            this.close();
            Ext.Msg.alert('Información', 'El Accesorio fue editado con Exito');
        }
    },

    validateDataAccessory: function (panel) {
        var dataValidate = true,
            accessoryForm = panel.down('#pnl-accessory'),
            accessoryImage = panel.down('#preview-image-btn'),
            gridComponents = panel.down('#pnl-grid-items');

        accessoryForm.items.each(function (field) {
            if(Spyd.util.Util.validateField(field) === false) {
                dataValidate = false;
                return;
            }
        });
        return dataValidate;
    },



    /**
     * Handle cleaned.
     * @private
     */
    handleClean: function () {
        var me = this,
            accessoryForm = me.down('#pnl-accessory'),
            accessoryImage = me.down('#preview-image-btn'),
            gridComponents = me.down('#pnl-grid-items');

        accessoryForm.items.get('nameId').setValue('');
        accessoryForm.items.get('descriptionId').setValue('');
        accessoryForm.items.get('ownerId').setValue('');
        accessoryForm.items.get('enterdateId').setValue(new Date());
        accessoryImage.image.dom.setAttribute('src', '/assets/accessories/sample-accessory.jpg');
        gridComponents.getStore().removeAll();
    },

    /**
     * Handle canceled.
     * @private
     */
    handleCancel: function () {
        var me = this,
        gridComponents = me.down('#pnl-grid-items');
        gridComponents.getStore().removeAll();
        me.close();
    }
});
