/**
 * @class Spyd.view.return.ReturnForm
 */
Ext.define('Spyd.view.return.ReturnForm', {
    requires: [
        'Spyd.view.return.ReturnElementGrid',
        'Spyd.store.Employee',
        'Spyd.store.Accessory'
    ],

    extend: 'Ext.window.Window',
    alias: 'widget.returnmanager',
    region: 'center',
    layout: 'fit',
    title: 'Registro de Devolución',
    overflow: true,
    overflowY: 'auto',
    height: 520,
    width: 600,
    cls: 'return-manager',
    itemId: 'return-manager',
    resizable: true,
    parentGridPanel: null,
    loanComponentStore: null,
    loanAccessoryModel: null,
    accessoryModel: null,
    defaults: {
        anchor: '100%',
        border: false
    },
 
    items: [{
        xtype: 'panel',
        itemId: 'return-form',
        cls: 'return-form',
        title: 'Devolucion Accesorio',
        collapsible: false,
        layout: {
            type: 'border'
        },
        items: [{
            xtype: 'panel',
            region:'center',
            margins: '5 0 0 0',
            itemId: 'pnl-return',
            cls: 'pnl-return',
            items: [{
                xtype: 'textfield',
                name: 'name',
                itemId: 'nameId',
                fieldLabel: 'Nombre',
                emptyText: 'Nombre del Accesorio',
                margin: '5 0 10 10',
                size: 30,
                allowBlank: false,
                readOnly: true
            }, {
                xtype: 'combo',
                name: 'employee',
                store: Ext.create('Spyd.store.Employee', {roleId: 3}).load(),
                fieldLabel: 'Empleado',
                itemId: 'employeeId',
                valueField: 'EmployeeId',
                displayField: 'FullName',
                emptyText: 'Seleccione un Empleado',
                queryMode: 'local',
                margin: '0 0 10 10',
                readOnly: true,
                size: 30
            }, {
                xtype: 'combo',
                name: 'responsible',
                store: Ext.create('Spyd.store.Employee').load(),
                fieldLabel: 'Responsable',
                itemId: 'responsibleId',
                valueField: 'EmployeeId',
                displayField: 'FullName',
                queryMode: 'local',
                margin: '0 0 10 10',
                readOnly: true,
                size: 30,
                listeners: {
                    select: function (combo, records) {
                    }
                }
            }, {
                xtype: 'datefield',
                name: 'enterdate',
                value: new Date(),
                itemId: 'returndateId',
                fieldLabel: 'Fecha de Devolución',
                margin: '0 0 10 10',
                readOnly: true,
                width: 320,
                handler: function(picker, date) {
                    Ext.Msg.alert('Date Selected', Ext.Date.format(date, 'M j, Y'));
                }
            },{
                xtype: 'textareafield',
                name: 'commets',
                itemId: 'commetsId',
                fieldLabel: 'Comentarios',
                emptyText: 'Comentarios de la Devolución',
                margin: '0 0 10 10',
                width:320
            }]
        }, {
            xtype: 'panel',
            region:'east',
            itemId: 'pnl-image',
            cls: 'pnl-image',
            margins: '5 0 0 0',
            height: 200,
            width: 250,
            
            items: [{
                xtype:'image',
                itemId: 'accesoryToReturn_image',
                src: null,
                width: 240,
                
            }]
        },{
            xtype: 'returncomponentsgrid',
            region: 'south',
            itemId: 'pnl-returngrid-items',
            cls: 'pnl-returngrid-items',
            height: 200,
            width: 500,
            cmargins: '5 0 0 0'
        }]
    }],

    /**
     * Button docked bar
     */
    fbar: [{
        text: 'Registrar',
        width: 75,
        handler: function (btn) {
            var w = btn.up('returnmanager');
            w.handleSaveReturnAccessory();
        }
    }, {
        text: 'Cancelar',
        width: 75,
        handler: function (btn) {
            var w = btn.up('returnmanager');
            w.handleCancel();
        }
    }],

    /**
     * Initialize component
     */
    initComponent: function () {
        var me = this;
        me.addEvents('returnLoanAccessory');
        me.callParent();
    },

    initLendingRegister: function () {
        var me = this;
    },

    initLendingEditor: function () {
        var me = this;
    },

    loadSelectedAccesoryforReturn: function(loanAccessory, accessory) {
        var me = this,
            returnForm = me.down('#pnl-return'),
            accessoryImage = me.down('#accesoryToReturn_image'),
            gridComponents = me.down('#pnl-returngrid-items'),
            imageFile = accessory.data.ImagePath === '' || accessory.data.ImagePath === null ?
                        '/assets/accessories/sample-accessory.jpg' :
                        '/assets/accessories/' + accessory.data.ImagePath;
        returnForm.items.get('nameId').setValue(accessory.data.Name);
        returnForm.items.get('employeeId').setValue(loanAccessory.data.EmployeeId);
        returnForm.items.get('responsibleId').setValue(loanAccessory.data.ResponsibleId);
        accessoryImage.src = imageFile;
        gridComponents.getStore().removeAll();
        me.loanComponentStore = Ext.create('Spyd.store.LoanComponent');
        me.loanComponentStore.proxy.url = Spyd.util.Util.formatArguments(me.loanComponentStore.proxy.url, [loanAccessory.data.LoanId]);
        me.loanComponentStore.load({
            scope: me,
            callback: function(componentStore) {
                componentStore.forEach(function(cmp) {
                    if (cmp.data.LoanId === loanAccessory.data.LoanId) {
                        gridComponents.getStore().insert(0, cmp);
                    }
                });
            }
        });
    },

    /**
     * Handle to save return loaned accessory.
     * @private
     */
    handleSaveReturnAccessory: function () {
        var me = this,
            validation = me.validateDataAccessory(me);
        if (validation === true) {
            me.fireEvent('returnLoanAccessory', me);
            this.close();
            Ext.Msg.alert('Información', 'La Devolución fue registrada con éxito');
        }
    },

    /**
     * Validate data fields to return an accessory
     * @private
     */
    validateDataAccessory: function (panel) {
        var dataValidate = true;
            accessoryForm = panel.down('#pnl-return'),
            accessoryImage = panel.down('#pnl-image'),
            gridComponents = panel.down('#pnl-returngrid-items');
        
            if(Spyd.util.Util.validateGrid(gridComponents) === false) {
                dataValidate = false;
                return;
            }
        return dataValidate;
    },

    /**
     * Handle canceled.
     * @private
     */
    handleCancel: function () {
        var me = this,
            gridComponents = me.down('#pnl-returngrid-items');
        gridComponents.getStore().removeAll();
        me.close();
    }
});
