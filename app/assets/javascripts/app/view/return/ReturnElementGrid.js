/**
 * @class Spyd.view.return.ReturnElementGrid
 */
Ext.define('Spyd.view.return.ReturnElementGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.returncomponentsgrid',
    
    requires: [
        'Spyd.store.Component',
        'Spyd.model.Component'
    ],

    xtype: 'cell-editing',
    title: 'Lista de Componentes',
    frame: true,
    
    initComponent: function() {
        this.cellEditing = new Ext.grid.plugin.CellEditing({
            clicksToEdit: 1
        });
        
        Ext.apply(this, {
            plugins: [this.cellEditing],
            columns: [{
                xtype: 'checkcolumn',
                header: 'Prestado',
                dataIndex: 'IsLoaned',
                width: 60,
                editor: {
                    xtype: 'checkbox',
                    cls: 'x-grid-checkheader-editor'
                }
            },{
                header: 'Nombre Componente',
                dataIndex: 'ComponentName',
                flex: 1,
                readOnly: true
            }, {
                header: 'Cantidad Prestada',
                dataIndex: 'LoanAmount',
                width: 120,
                align: 'right',
                readOnly: true
            }, {
                header: 'Cantidad Devuelta',
                dataIndex: 'ReturnAmount',
                width: 120,
                align: 'right',
                editor: {
                    xtype: 'numberfield',
                    allowBlank: false,
                    minValue: 0,
                    maxValue: 100000
                }
            }],
        });
        
        this.callParent();
    },
    
    /*onAddClick: function() {
        var newComponent = Ext.create('Spyd.model.LoanComponent', {});
        this.getStore().insert(0, newComponent);
        this.cellEditing.startEditByPosi tion({
            row: 0, 
            column: 0
        });
    },*/
});