/**
 * @class Spyd.view.MainControl
 */
Ext.define('Spyd.view.MainControl', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.controlgridview',
    
    requires: [
        'Spyd.util.grid.ActionButtonColumn',
        'Spyd.store.AccessoryView',
        'Spyd.store.Accessory',
        'Spyd.store.Component',
        'Spyd.store.Employee',
    ],

    xtype: 'cell-editing',
    itemId: 'control-grid-view',
    cls: 'control-grid-view',
    title: 'Estado de Accesorios ',
    store: Ext.create('Spyd.store.AccessoryView'),
    accessoryModel: null,
    frame: true,
    
    initComponent: function() {
        var me = this;
        me.store.load();

        me.cellEditing = new Ext.grid.plugin.CellEditing({
            clicksToEdit: 1
        });

        Ext.apply(this, {
            columns: [{
                header: 'Nombre Accesorio',
                dataIndex: 'AccessoryName',
                flex: 1,
                editor: {
                    allowBlank: false
                }
            }, {
                header: 'Nombre Empleado',
                dataIndex: 'EmployeeName',
                flex: 1,
                editor: {
                    allowBlank: false
                }
            }, {
                header: 'Fecha de Prestamo',
                dataIndex: 'LoanDate',
                flex: 1,
                editor: {
                    allowBlank: false
                }
            }, {
                header: 'Hora de Prestamo',
                dataIndex: 'LoanHour',
                flex: 1,
                editor: {
                    allowBlank: false
                }
            }, {
                header: 'Próxima Hora Reservada',
                dataIndex: 'NextReserveHour',
                flex: 1,
                editor: {
                    allowBlank: false
                }
            }, {
                header: 'Estado Accesorio',
                dataIndex: 'Status',
                flex: 1,
                editor: {
                    allowBlank: false
                }
            }, {
                header: 'Accion',
                xtype: 'actionbuttoncolumn',
                align: 'center',
                dataIndex: 'Status',
                items: [{
                    itemId: 'loan-item',
                    text: 'Prestar',
                    tooltip: 'Prestar',
                    handler: function(gridPanel, rowIndex, colIndex) {
                        me.fireEvent('loanAccessoryAction', gridPanel, rowIndex);
                    }
                }, {
                    itemId: 'return-loanedItem',
                    text: 'Devolver',
                    tooltip: 'Devolver',
                    handler: function(gridPanel, rowIndex, colIndex) {
                        me.fireEvent('returnAccessoryAction', gridPanel, rowIndex);
                    }
                }]
            }]
        });

        me.addEvents('loanAccessoryAction');
        me.addEvents('returnAccessoryAction');
        me.callParent();
    }
});