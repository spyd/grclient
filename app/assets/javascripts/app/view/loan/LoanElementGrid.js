/**
 * @class Spyd.view.loan.LoanElementGrid
 */
Ext.define('Spyd.view.loan.LoanElementGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.lendcomponentsgrid',
    
    requires: [
        'Spyd.store.Component',
        'Spyd.model.Component'
    ],

    xtype: 'cell-editing',
    title: 'Lista de Componentes',
    frame: true,
    
    initComponent: function() {
        this.cellEditing = new Ext.grid.plugin.CellEditing({
            clicksToEdit: 1
        });
        
        Ext.apply(this, {
            plugins: [this.cellEditing],
            columns: [{
                xtype: 'checkcolumn',
                header: 'Prestado',
                dataIndex: 'IsLoaned',
                width: 60,
                editor: {
                    xtype: 'checkbox',
                    cls: 'x-grid-checkheader-editor',
                    checked: true
                }
            },{
                header: 'Nombre Componente',
                dataIndex: 'ComponentName',
                flex: 1,
                readOnly: true,
            }, {
                header: 'Cantidad Prestada',
                dataIndex: 'Amount',
                width: 120,
                align: 'right',
                editor: {
                    xtype: 'numberfield',
                    allowBlank: false,
                    minValue: 0,
                    maxValue: 100000
                }
            }],
        });
        
        this.callParent();
    },
    
    loadLoanStore: function() {
        //this.getStore().load();
    },
    
    onAddClick: function(){
        var newComponent = Ext.create('Spyd.model.Component', {});
        this.getStore().insert(0, newComponent);
        this.cellEditing.startEditByPosition({
            row: 0, 
            column: 0
        });
    },
});