/**
 * @class Spyd.view.loan.LoanAccessoryForm
 */
Ext.define('Spyd.view.loan.LoanAccessoryForm', {
    requires: [
        'Spyd.view.loan.LoanElementGrid',
        'Spyd.store.Employee',
        'Spyd.store.Accessory'
    ],

    extend: 'Ext.window.Window',
    alias: 'widget.lendingmanager',
    region: 'center',
    layout: 'fit',
    title: 'Registro de Préstamo',
    overflow: true,
    overflowY: 'auto',
    height: 470,
    width: 600,
    cls: 'lending-manager',
    itemId: 'lending-manager',
    resizable: true,
    parentGridPanel: null,
    accessoryModel: null,
    nextHourReserved: false,
    defaults: {
        anchor: '100%',
        border: false
    },

    /**
     * Initialize component
     */
    initComponent: function () {
        var me = this;

        me.items = [{
            xtype: 'panel',
            itemId: 'lending-form',
            cls: 'lending-form',
            title: 'Nuevo Préstamo',
            collapsible: false,
            layout: {
                type: 'border'
            },
            items: [{
                xtype: 'panel',
                region:'center',
                margins: '5 0 0 0',
                itemId: 'pnl-lending',
                cls: 'pnl-lending',
                items: [{
                    xtype: 'textfield',
                    name: 'name',
                    itemId: 'nameId',
                    fieldLabel: 'Nombre',
                    emptyText: 'Nombre del Accesorio',
                    margin: '5 0 10 10',
                    size: 30,
                    allowBlank: false,
                    readOnly: true
                }, {
                    xtype: 'combo',
                    name: 'employee',
                    store: Ext.create('Spyd.store.Employee', {roleId: 3}).load(),
                    fieldLabel: 'Empleado',
                    itemId: 'employeeId',
                    valueField: 'EmployeeId',
                    displayField: 'FullName',
                    emptyText: 'Seleccione Empleado a prestar',
                    queryMode: 'local',
                    allowBlank: false,
                    editable: true,
                    forceSelection: true,
                    typeAhead: true,
                    margin: '0 0 10 10',
                    size: 30,
                    listeners: {
                        select: function (combo, records) {
                        }
                    }
                }, {
                    xtype: 'combo',
                    name: 'responsible',
                    store: Ext.create('Spyd.store.Employee').load(),
                    fieldLabel: 'Responsable',
                    itemId: 'responsibleId',
                    valueField: 'EmployeeId',
                    displayField: 'FullName',
                    queryMode: 'local',
                    margin: '0 0 10 10',
                    editable: false,
                    readOnly: true,
                    size: 30,
                    listeners: {
                        select: function (combo, records) {
                        }
                    }
                }, {
                    xtype: 'datefield',
                    name: 'enterdate',
                    value: new Date(),
                    itemId: 'enterdateId',
                    fieldLabel: 'Fecha de Préstamo',
                    margin: '0 0 10 10',
                    readOnly: true,
                    width: 320,
                    handler: function(picker, date) {
                        Ext.Msg.alert('Date Selected', Ext.Date.format(date, 'M j, Y'));
                    }
                }, {
                    xtype: 'label',
                    itemId: 'nextReserveId',
                    hidden: me.nextHourReserved === false,
                    text: 'Accesorio Reservado',
                    margin: '0 0 10 10'
                }]
            }, {
                //imagen del accesorio
                xtype: 'panel',
                region:'east',
                itemId: 'pnl-image',
                cls: 'pnl-image',
                margins: '5 0 0 0',
                height: 200,
                width: 250,
                items: [{
                    xtype:'image',
                    itemId: 'accesoryToLend_image',
                    src: null,
                    width: 240,
                    
                }]
            },{
                xtype: 'lendcomponentsgrid',
                region: 'south',
                itemId: 'pnl-loangrid-items',
                cls: 'pnl-loangrid-items',
                height: 200,
                width: 500,
                cmargins: '5 0 0 0'
            }]
        }];

        me.fbar = [{
            text: 'Registrar',
            //disabled: me.nextHourReserved,
            width: 75,
            handler: function (btn) {
                var w = btn.up('lendingmanager');
                w.handleSaveLoanAccessory();
            }
        }, {
            text: 'Cancelar',
            width: 75,
            handler: function (btn) {
                var w = btn.up('lendingmanager');
                w.handleCancel();
            }
        }];

        me.addEvents('createLoanAccessory');
        me.callParent();
    },

    initLendingRegister: function () {
        var me = this;
    },

    initLendingEditor: function () {
        var me = this;
    },

    loadSelectedAccesoryforLend: function(selectedAccessory, componentStore, message) {
        var me = this,
            lendForm = me.down('#pnl-lending'),
            accessoryImage = me.down('#accesoryToLend_image'),
            gridComponents = me.down('#pnl-loangrid-items'),
            imageFile = selectedAccessory.data.ImagePath === '' || selectedAccessory.data.ImagePath === null ?
                        '/assets/accessories/sample-accessory.jpg' :
                        '/assets/accessories/' + selectedAccessory.data.ImagePath;
        lendForm.items.get('nameId').setValue(selectedAccessory.data.Name);
        lendForm.items.get('responsibleId').setValue(Ext.getCmp('spyd-main-panel').userIdSession);
        lendForm.items.get('nextReserveId').setText(message);
        accessoryImage.src = imageFile;
        gridComponents.getStore().removeAll();
        componentStore.load({
            scope: me,
            callback: function(componentStore) {
                componentStore.forEach(function(cmp) {
                    if (cmp.data.AccessoryId === selectedAccessory.data.Id) {
                        cmp.data.IsLoaned = true;
                        gridComponents.getStore().insert(0, cmp);
                    }
                });
            }
        });
    },

    /**
     * Handle to save loan accessory.
     * @private
     */
    handleSaveLoanAccessory: function () {
        var me = this,
            gridComponents = me.down('#pnl-loangrid-items'),
            validation = me.validateDataAccessory(me);
        if (validation === true) {
            me.fireEvent('createLoanAccessory', me);
            this.close();
            Ext.Msg.alert('Información', 'El Préstamo fue registrado con éxito');
        }
    },

    /**
     * Validate data fields to loan an accessory
     * @private
     */
    validateDataAccessory: function (panel) {
            var dataValidate = true;
            accessoryForm = panel.down('#pnl-lending'),
            accessoryImage = panel.down('#pnl-image'),
            gridComponents = panel.down('#pnl-loangrid-items');

        accessoryForm.items.each(function (field) {
            if(Spyd.util.Util.validateField(field) === false) {
                dataValidate = false;
                return;
            }
        });
        return dataValidate;
    },

    /**
     * Handle canceled.
     * @private
     */
    handleCancel: function () {
        var me = this,
            gridComponents = me.down('#pnl-loangrid-items');
        gridComponents.getStore().removeAll();
        me.close();
    }
});
