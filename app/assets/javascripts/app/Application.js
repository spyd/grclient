Ext.Loader.setPath('Ext', '/assets/extjs-4.2.0.663/src');

Ext.Loader.setConfig({
    enabled: true
});

/**
 * Represents a ExtJS application, this is the entry
 * point for Spyd project.
 */
Ext.application({
    name: 'Spyd',
    appFolder: '/assets/app',
    autoCreateViewport: true,
    requires: [
        'Spyd.store.Account',
        'Spyd.store.AccessoryView',
        'Spyd.model.AccessoryView',
        'Spyd.util.calendar.AbstractCalendarOverride',
        'Spyd.util.calendar.EventWindow',
        'Spyd.util.calendar.menu.Event',
        'Spyd.util.calendar.DateRange'
    ],

    /**
     * @todo Plugin controllers should be dynamically injected, probably
     * using a plugins store that loads registered plugins
     */
    controllers: [
        'Spyd.controller.Accessory',
        'Spyd.controller.MainControl',
        'Spyd.controller.LoanAccessory',
        'Spyd.controller.Reserve'
    ],

    launch: function () {
        // Nothing to do.
    }
});