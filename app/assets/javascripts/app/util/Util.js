/**
 * @class Spyd.util.Util
 */
Ext.define('Spyd.util.Util', {
    singleton: true,

    /*
     * Validates a specific field component
     */
    validateField: function (field) {
    	var dataFieldOk, message = "",
    		dataValidate = true;

        if (field.xtype === "label") {
            return dataValidate;
        }

        if (field.allowBlank === false) {
            if (field.getValue() === '') {
                message = Ext.String.format('El campo {0} del Accesorio no puede ser vacio.', field.fieldLabel);
                Ext.Msg.alert('Información', message);
                dataValidate = false;
                return dataValidate;
            }
        }
        if(field.getValue() !== ''){
            if (field.regex !== undefined) {
                dataFieldOk = field.regex.test(field.getValue());
                if (dataFieldOk === false) {
                    message = Ext.String.format('El campo {0} {1}', field.fieldLabel, field.regexText);
                    Ext.Msg.alert('Información', message);
                    dataValidate = false;
                    return dataValidate;
                }
            }
            if (field.maxLenght !== undefined){
                if (field.getValue().length>field.maxLenght){
                    message = Ext.String.format('En el campo {0} {1}', field.fieldLabel, field.maxLenghtText);
                    Ext.Msg.alert('Información', message);
                    dataValidate = false;
                    return dataValidate;
                }
            }
        }
        if (field.getValue() === undefined) {
            message = Ext.String.format('El campo {0} no puede ser vacio', field.fieldLabel);
            Ext.Msg.alert('Información', message);
            dataValidate = false;
            return dataValidate;
        }
        if (field.getValue() === null) {
            message = Ext.String.format('El campo {0} no puede ser vacio', field.fieldLabel);
            Ext.Msg.alert('Información', message);
            dataValidate = false;
            return dataValidate;
        }
        //return dataValidate;
    },

    validateGrid: function (grid){
        var me=this,
            dataGridOk, message = "",
            dataGridOk = true;

        if (grid.getStore().getCount()===0){
            Ext.Msg.alert('Información', 'El Accesorio debe tener al menos un componente registrado');
            dataGridOk = false;
        } else {
            /*grid.getStore().each(function (field){
                if(me.validateField(field) === false) {
                dataGridOk = false;
                return;
            }
            });
            return dataGridOk;*/
        }

        return dataGridOk;
    },

    /*
     * Gets base url from current page
     */
    baseUrl: function () {
        if (!window.location.origin) {
            window.location.origin = window.location.protocol + "//" + window.location.hostname + (window.location.port ? ':' + window.location.port : '');
        }
        return window.location.origin;
    },

    formatArguments: function (format, args) {
        if (Ext.isArray(args)) {
            for (var i = 0; i < args.length; i++) {
                format = format.replace('{' + i + '}', args[i]);
            }
        }
        return format;
    }
});