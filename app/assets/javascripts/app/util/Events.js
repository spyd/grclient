/*!
 * Extensible 1.5.2
 * Copyright(c) 2010-2013 Extensible, LLC
 * licensing@ext.ensible.com
 * http://ext.ensible.com
 */
Ext.define('Spyd.util.Events', {
    constructor :  function() {
        var today = Ext.Date.clearTime(new Date),
            makeDate = function(d, h, m, s){
                d = d * 86400;
                h = (h || 0) * 3600;
                m = (m || 0) * 60;
                s = (s || 0);
                return Ext.Date.add(today, Ext.Date.SECOND, d + h + m + s);
            };
            
        return {
            "evts" : [{
                "id"    : 1001,
                "cid"   : 1,
                "title" : "Futbolin de Madera",
                "start" : makeDate(3, 10),
                "end"   : makeDate(3, 15),
                "notes" : ""
            },{
                "id"    : 1002,
                "cid"   : 2,
                "title" : "Mesa de Billar",
                "start" : makeDate(0, 11, 30),
                "end"   : makeDate(0, 13),
                "notes" : ""
            },{
                "id"    : 1003,
                "cid"   : 3,
                "title" : "Mesa de Billar",
                "start" : makeDate(1, 15),
                "end"   : makeDate(1, 15),
                "notes" : ""
            },{
                "id"    : 1006,
                "cid"   : 3,
                "title" : "Futbolin de Madera",
                "start" : makeDate(-1, 15),
                "end"   : makeDate(-1, 19),
                "notes" : ""
            },{
                "id"    : 1007,
                "cid"   : 1,
                "title" : "Futbolin de Madera",
                "start" : makeDate(0, 9),
                "end"   : makeDate(0, 9, 30),
                "notes" : "completo"
            },{
                "id"    : 1009,
                "cid"   : 2,
                "title" : "Futbolin",
                "start" : makeDate(-2, 13),
                "end"   : makeDate(-2, 18),
                "notes" : ""
            },{
                "id"    : 1011,
                "cid"   : 1,
                "title" : "Xbox 2013",
                "start" : makeDate(2, 19),
                "end"   : makeDate(2, 23),
                "notes" : ""
            }]
        }
    }
});