Ext.define('Spyd.util.calendar.EventWindow', {
    override: 'Extensible.calendar.form.EventWindow',
    
    // Locale configs
    title: 'Reserva de Accesorio',
    titleTextAdd: 'Adicionar Reserva',
    titleTextEdit: 'Editar Reserva',
    width: 600,
    height: 265,
    labelWidth: 65,
    savingMessage: 'Grabando reserva de accesorio...',
    deletingMessage: 'Eliminando reserva...',
    saveButtonText: 'Registrar',
    deleteButtonText: 'Eliminar',
    cancelButtonText: 'Cancelar',
    titleLabelText: 'Titulo',
    datesLabelText: 'Hasta',
    calendarLabelText: 'Calendar',
    accessoryName: '',
    imageFile: '',
    employeeId: 0,
    accessoryId: 0,

    //initComponent: function()
    //{
    //    this.callParent();
    //},

    getFormItemConfigs: function() {
        var items = [{
            xtype: 'panel',
            itemId: 'reserve-form',
            cls: 'reserve-form',
            //collapsible: false,
            height: 200,
            layout: {
                type: 'border'
            },
            items: [{
                xtype: 'panel',
                region:'center',
                margins: '5 0 0 0',
                itemId: 'pnl-reserve',
                cls: 'pnl-reserve',
                items: [{
                    xtype: 'textfield',
                    name: 'name',
                    //itemId: 'nameId',
                    itemId: this.id + '-title',
                    fieldLabel: 'Nombre',
                    emptyText: 'Nombre del Accesorio',
                    size: 30,
                    readOnly: true
                }, {
                    xtype: 'combo',
                    name: 'employee',
                    store: Ext.create('Spyd.store.Employee', {roleId: 3}).load(),
                    fieldLabel: 'Empleado',
                    itemId: this.id + '-user',
                    valueField: 'EmployeeId',
                    displayField: 'FullName',
                    emptyText: 'Empleado a reservar accesorio',
                    queryMode: 'local',
                    size: 29,
                    readOnly: true
                }, {
                    xtype: 'datefield',
                    name: 'currentdate',
                    value: new Date(),
                    itemId: 'reservedateId',
                    fieldLabel: 'Fecha de Reserva',
                    width: 280,
                    readOnly: true
                }, {
                    xtype: 'timefield',
                    name: 'initialdate',
                    fieldLabel: 'Desde',
                    itemId: 'initialdateId',
                    minValue: '8:00 AM',
                    maxValue: '8:00 PM',
                    width: 190,
                    format: Extensible.Date.use24HourTime ? 'G:i' : 'g:i A',
                    readOnly: true
                    //disabled: true
                }, {
                    xtype: 'extensible.daterangefield',
                    //itemId: 'pnl-range-dates',
                    itemId: this.id + '-dates',
                    cls: 'pnl-range-dates',
                    name: 'dates',
                    toText: 'Hasta',
                    //margins: '25 0 0 0',
                    //anchor: '95%',
                    singleLine: true,
                    fieldLabel: this.datesLabelText,
                    getAllDayConfig: function() { return {}; }
                }]
            }, {
                //imagen del accesorio
                xtype: 'panel',
                region:'east',
                itemId: 'pnl-image',
                cls: 'pnl-image',
                margins: '5 0 0 0',
                height: 200,
                width: 250,
                items: [{
                    xtype:'image',
                    itemId: 'accesory-image',
                    src: '/assets/accessories/sample-accessory.jpg',
                    width: 240,
                }]            
            }]
        }];

        if(this.calendarStore){
            items.push({
                xtype: 'extensible.calendarcombo',
                itemId: this.id + '-calendar',
                name: Extensible.calendar.data.EventMappings.CalendarId.name,
                anchor: '100%',
                fieldLabel: this.calendarLabelText,
                store: this.calendarStore
            });
        }

        return items;
    },

    getFooterBarConfig: function() {
        var cfg = [{
                hidden: true,
            }, '->', {
                text: this.saveButtonText,
                itemId: this.id + '-save-btn',
                disabled: false,
                handler: this.onSave, 
                scope: this
            }, {
                text: this.cancelButtonText,
                itemId: this.id + '-cancel-btn',
                disabled: false,
                handler: this.onCancel,
                scope: this
            }];

        return cfg;
    },

    // private
    onRender : function(ct, position){        
        this.formPanel = Ext.create('Ext.form.Panel', Ext.applyIf({
            fieldDefaults: {
                labelWidth: this.labelWidth
            },
            items: this.getFormItemConfigs()
        }, this.formPanelConfig));
        
        this.add(this.formPanel);
        
        this.callParent(arguments);
    },

    // private
    afterRender: function(){
        this.callParent(arguments);
        
        this.el.addCls('ext-cal-event-win');
        
        this.initRefs();

        // This junk spacer item gets added to the fbar by Ext (fixed in 4.0.2)
        var junkSpacer = this.getDockedItems('toolbar')[0].items.items[0];
        if (junkSpacer.el.hasCls('x-component-default')) {
            Ext.destroy(junkSpacer);
        }
    },
    
    initRefs: function() {
        // toolbar button refs
        this.saveButton = this.down('#' + this.id + '-save-btn');
        //this.deleteButton = this.down('#' + this.id + '-delete-btn');
        this.cancelButton = this.down('#' + this.id + '-cancel-btn');
        
        // form item refs
        this.titleField = this.down('#' + this.id + '-title');
        this.employeeField = this.down('#' + this.id + '-user');
        this.reserveDateField = this.down('#reservedateId');
        this.imageField = this.down('#accesory-image');
        this.dateRangeField = this.down('#' + this.id + '-dates');
        this.initialdateId = this.down('#initialdateId');
        this.calendarField = this.down('#' + this.id + '-calendar');

        if (this.activeRecord !== null) {
            this.dateRangeField.items.items[0].style = {"display":"none"};
            this.dateRangeField.items.items[1].style = {"display":"none"};
            this.dateRangeField.items.items[2].style = {"display":"none"};
            this.dateRangeField.items.items[3].width = 120;
            this.dateRangeField.items.items[3].setMinValue('8:00 AM');
            this.dateRangeField.items.items[3].setMaxValue('8:00 PM');
            this.dateRangeField.items.items[4].style = {"display":"none"};
            this.updateReserveFields();
        }
    },

    updateReserveFields: function() {
        this.dateRangeField.setValue(this.activeRecord.data);
        this.employeeField.setValue(this.employeeId);
        this.titleField.setValue(this.accessoryName);
        this.reserveDateField.setValue(this.activeRecord.data.StartDate);
        this.initialdateId.setValue(this.dateRangeField.items.items[1].getValue());
        this.imageField.setSrc(this.imageFile);
    },

    // private
    onSave: function(){
        if(!this.formPanel.form.isValid()){
            return;
        }
        if(!this.updateRecord(this.activeRecord)){
            this.onCancel();
            return;
        }
        if (this.activeRecord.data.StartDate < this.activeRecord.data.EndDate) {
            this.saveReserveAccessory();
            this.fireEvent(this.activeRecord.phantom ? 'eventadd' : 'eventupdate', this, this.activeRecord, this.animateTarget);
        } else {
            var msg = Ext.String.format('Hora Final {0} seleccionada no debe ser antes o igual de la Hora Inicial.',
                        Ext.Date.format(this.activeRecord.data.EndDate, 'H:i:s'));
            Ext.Msg.alert('Informacion', msg);
        }
    },

    saveReserveAccessory: function() {
        var me = this, currentdate = new Date(),
            searchListPnl = Ext.getCmp('search-accessory-list-reserve'),
            listReservesPnl = searchListPnl.up('#main-panel').down('#reserve-accessory').down('#reserve-calendar'),
            myReservesPnl = searchListPnl.up('#main-panel').down('#my-reserve-list').down('#reserve-calendar'),
            reserve = Ext.create('Spyd.model.Reserve', {
                EmployeeId: this.employeeId,
                AccessoryId: this.accessoryId,
                InitialDate: this.activeRecord.data.StartDate,
                InitialHour: Ext.Date.format(this.activeRecord.data.StartDate, 'h:i:s'),
                FinalDate: this.activeRecord.data.EndDate,
                FinalHour: Ext.Date.format(this.activeRecord.data.EndDate, 'h:i:s'),
                ReserveDate: currentdate,
                ReserveHour: Ext.Date.format(currentdate, 'h:i:s'),
                Confirmed: false,
                Canceled: false,
                Note: ''
            });
        reserve.save();
        myReservesPnl.loadMyReservesCalendar(searchListPnl.store, this.employeeId);
        listReservesPnl.loadReservesCalendar(searchListPnl.accessoryModel);
    },

    initializeReserveForm: function(o, searchListPnl) {
        var M = Extensible.calendar.data.EventMappings,
            mainPanel = searchListPnl.up('#main-panel'),
            accessorySelected = Ext.select('.selected-item').elements[0],
            start = o[M.StartDate.name],
            end = o[M.EndDate.name] || Extensible.Date.add(start, {hours: 1}),
            rec = Ext.create('Extensible.calendar.data.EventModel');
        
        this.setTitle(this.titleTextAdd);
        this.accessoryName = accessorySelected.childNodes[0].textContent;
        this.imageFile = searchListPnl.accessoryModel.data.ImagePath === '' || searchListPnl.accessoryModel.data.ImagePath === null ?
                            '/assets/accessories/sample-accessory.jpg' :
                            '/assets/accessories/' + searchListPnl.accessoryModel.data.ImagePath;
        this.employeeId = mainPanel.userIdSession;
        this.accessoryId = searchListPnl.accessoryModel.data.Id;

        //rec.data[M.EventId.name] = this.newId++;
        rec.data[M.Title.name] = Ext.String.format('<br> {0} <br/> Note: ', this.accessoryName);
        rec.data[M.StartDate.name] = start;
        rec.data[M.EndDate.name] = end;
        rec.data[M.IsAllDay.name] = !!o[M.IsAllDay.name] || start.getDate() != Extensible.Date.add(end, {millis: 1}).getDate();

        this.activeRecord = rec;

        if (this.formPanel !== undefined && this.dateRangeField !== undefined) {
            this.updateReserveFields();
        }

        return this;
    }
});