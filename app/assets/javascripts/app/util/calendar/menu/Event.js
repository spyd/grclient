
Ext.define('Spyd.util.calendar.menu.Event', {
    override: 'Extensible.calendar.menu.Event',

    initComponent : function(){
        this.addEvents(
            /**
             * @event confirmreserve
             */
            'confirmreserve',
            /**
             * @event cancelreserve
             */
            'cancelreserve'
        );
        this.buildMenu();
        this.callParent(arguments);
    },

    buildMenu: function(){
        if(this.rendered){
            return;
        }
        this.dateMenu = Ext.create('Ext.menu.DatePicker', {
            scope: this,
            handler: function(dp, dt){
                dt = Extensible.Date.copyTime(this.rec.data[Extensible.calendar.data.EventMappings.StartDate.name], dt);
                this.fireEvent('eventmove', this, this.rec, dt);
            }
        });
        
        Ext.apply(this, {
            items: [{
                text: 'Confirmar Reserva',
                iconCls: 'extensible-cal-icon-evt-edit',
                itemId: 'confirm-option-menu',
                scope: this,
                handler: function(){
                    this.onConfirmReserve(this, this.rec, this.ctxEl);
                }
            },{
                text: 'Cancelar Reserva',
                iconCls: 'extensible-cal-icon-evt-del',
                itemId: 'cancel-option-menu',
                scope: this,
                handler: function(){
                    this.onCancelReserve(this, this.rec, this.ctxEl);
                }
            }]
        });
    },

    showForEvent: function(rec, el, xy){
        var currentdate = new Date();
        if (currentdate > rec.raw.Reserve.get('InitialDate') && Ext.getCmp('spyd-main-panel').adminMode === false) {
            return;
        }
    	if (rec.raw.Reserve.get('Confirmed') === false && rec.raw.Reserve.get('Canceled') === false) {
	        this.rec = rec;
	        this.ctxEl = el;
	        this.dateMenu.picker.setValue(rec.data[Extensible.calendar.data.EventMappings.StartDate.name]);
	        this.showAt(xy);
            if (rec.raw.Reserve.get('EmployeeId') === rec.raw.EmployeeId) {
                this.down('#confirm-option-menu').setVisible(false);
                this.down('#confirm-option-menu').setDisabled(true);
            }
	    }
    },
    
    onConfirmReserve: function(panel, record, el) {
    	var title = Ext.String.format(' (Reserva Confirmada) <br/> {0}', record.get('Title')),
            searchListPnl = Ext.getCmp('search-accessory-list-reserve'),
            reservesPnl = searchListPnl.up('#main-panel').down('#manage-reserve-list').down('#reserve-calendar');

    	record.set('Title', title);
        record.raw.Reserve.set('Confirmed', true);
        record.raw.Reserve.set('ResponsibleId', record.raw.EmployeeId);
        record.raw.Reserve.save();
        reservesPnl.loadListReservesCalendar(searchListPnl.store, record.raw.EmployeeId);
        Ext.Msg.alert('Informacion', 'Reserva confirmada satisfactoriamente.');
    },

    onCancelReserve: function(panel, record, el) {
    	var title = Ext.String.format(' (Reserva Cancelada) <br/> {0}', record.get('Title')),
            searchListPnl = Ext.getCmp('search-accessory-list-reserve');
        Ext.MessageBox.show({
            title:'Cancelacion de Reserva',
            msg: 'Esta a punto de cancelar la reserva seleccionada. <br/> Esta seguro?',
            buttons: Ext.MessageBox.YESNO,
            buttonText:{ 
                yes: "Si", 
                no: "No" 
            },
            fn: function(btn){
                var reservesPnl;
                if (btn == "yes") {
			    	record.set('Title', title);
			        record.raw.Reserve.set('Canceled', true);
			        record.raw.Reserve.save();
                    if (record.raw.Reserve.get('EmployeeId') === record.raw.EmployeeId) {
                        reservesPnl = searchListPnl.up('#main-panel').down('#my-reserve-list').down('#reserve-calendar');
                        reservesPnl.loadMyReservesCalendar(searchListPnl.store, record.raw.EmployeeId);
                    } else {
                        reservesPnl = searchListPnl.up('#main-panel').down('#manage-reserve-list').down('#reserve-calendar');
                        reservesPnl.loadListReservesCalendar(searchListPnl.store, record.raw.EmployeeId);
                    }
                }
            },
            icon: Ext.MessageBox.QUESTION
        });
    }
});