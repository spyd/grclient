
Ext.define('Spyd.util.calendar.DateRange', {
    override: 'Extensible.form.field.DateRange',

    fieldLayout: {
        type: 'vbox',
        defaultMargins: { top: 0, right: 0, bottom: 0, left: 0 }
    },

    getValue: function(){
        return [
            this.getDT('start'), 
            this.getDT('end')
        ];
    },
    
    setValue: function(v){
        if(!v) {
            return;
        }
        var me = this,
            eventMappings = Extensible.calendar.data.EventMappings,
            startDateName = eventMappings.StartDate.name;
            
        if(Ext.isArray(v)){
            me.setDT(v[0], 'start');
            me.setDT(v[1], 'end');
            //me.allDay.setValue(!!v[2]);
        }
        else if(Ext.isDate(v)){
            me.setDT(v, 'start');
            me.setDT(v, 'end');
            //me.allDay.setValue(false);
        }
        else if(v[startDateName]){ //object
            me.setDT(v[startDateName], 'start');
            if(!me.setDT(v[eventMappings.EndDate.name], 'end')){
                me.setDT(v[startDateName], 'end');
            }
            //me.allDay.setValue(!!v[eventMappings.IsAllDay.name]);
        }
    }
});