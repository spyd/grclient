Ext.define('Spyd.util.calendar.AbstractCalendarOverride', {
    override: 'Extensible.calendar.view.AbstractCalendar',

    showEventEditor : function(o, animateTarget){
        var currentdate = new Date(),
            searchListPnl = Ext.getCmp('search-accessory-list-reserve');
        if (currentdate <= o.StartDate) {
            if (searchListPnl.accessoryModel !== null) {
                if (animateTarget === null) {
                    this.getEventEditor().initializeReserveForm(o, searchListPnl, this);
                    this.getEventEditor().show(o, animateTarget, this);
                }
            } else {
                Ext.Msg.alert('Informacion', 'Seleccione accesorio a reservar en lista de accesorios.');
            }
        } else {
            Ext.Msg.alert('Informacion', 'Fecha seleccionada no esta disponible o fuera del dia u hora actual.');
        }
        return this;
    }

    /*getEventEditor : function()
    {
        this.editWin = this.ownerCalendarPanel.editWin;

        if(!this.editWin)
        {
            //Change this line:
            this.ownerCalendarPanel.editWin = Ext.create('Spyd.util.calendar.EventWindow', {
                id: 'ext-cal-editwin',
                calendarStore: this.calendarStore,
                modal: this.editModal,
                enableEditDetails: this.enableEditDetails,
                listeners: {
                    'eventadd': {
                        fn: function(win, rec, animTarget) {
                            //win.hide(animTarget);
                            win.currentView.onEventAdd(null, rec);
                        },
                        scope: this
                    },
                    'eventupdate': {
                        fn: function(win, rec, animTarget) {
                            //win.hide(animTarget);
                            win.currentView.onEventUpdate(null, rec);
                        },
                        scope: this
                    },
                    'eventdelete': {
                        fn: function(win, rec, animTarget) {
                            //win.hide(animTarget);
                            win.currentView.onEventDelete(null, rec);
                        },
                        scope: this
                    },
                    'eventcancel': {
                        fn: function(win, rec, animTarget){
                            this.dismissEventEditor(null, animTarget);
                            win.currentView.onEventCancel();
                        },
                        scope: this
                    }
                }
            });
        }

        // allows the window to reference the current scope in its callbacks
        this.editWin.currentView = this;
        return this.editWin;
    }*/
});