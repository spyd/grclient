/**
 * @class Spyd.store.AccessoryView
 * @extends Ext.data.Store
 * AccessoryView store class.
 */
Ext.define('Spyd.store.AccessoryView', {
    extend: 'Ext.data.Store',
    model: 'Spyd.model.AccessoryView',
    autoLoad: false,

    proxy: {
        type: 'rest',
        url: '/spyd/view/accessories',
        extraParams: {
            adapter_id: 1
        },
        reader: {
            type: 'json',
            root: 'data'
        }
    }
});