/**
 * @class Spyd.store.Role
 * @extends Ext.data.Store
 * Role store class.
 */
Ext.define('Spyd.store.Role', {
    extend: 'Ext.data.Store',
    model: 'Spyd.model.Role',
    autoLoad: false,
    proxy: {
        type: 'rest',
        url: '/spyd/view/accessories',
        extraParams: {
            adapter_id: 1
        },
        reader: {
            type: 'json',
            root: 'data'
        }
    }
});