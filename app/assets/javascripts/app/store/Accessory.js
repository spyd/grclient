/**
 * @class Spyd.store.Accessory
 * @extends Ext.data.Store
 * Accessory store class.
 */
Ext.define('Spyd.store.Accessory', {
    extend: 'Ext.data.Store',
    model: 'Spyd.model.Accessory',
    autoLoad: false,

    proxy: {
        type: 'rest',
        url: '/spyd/accessories',
        extraParams: {
            adapter_id: 1
        },
        reader: {
            type: 'json',
            root: 'data'
        }
    }
});