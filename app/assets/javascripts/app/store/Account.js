/**
 * @class Spyd.store.Account
 * @extends Ext.data.Store
 * Account store class.
 */
Ext.define('Spyd.store.Account', {
    extend: 'Ext.data.Store',
    model: 'Spyd.model.Account',
    autoLoad: true,
    proxy: {
        type: 'rest',
        url: '/spyd/accounts',
        extraParams: {
            adapter_id: 1
        },
        reader: {
            type: 'json',
            root: 'data'
        }
    }
});