/**
 * @class Spyd.store.LoanComponent
 * @extends Ext.data.Store
 * LoanComponent store class.
 */
Ext.define('Spyd.store.LoanComponent', {
    extend: 'Ext.data.Store',
    model: 'Spyd.model.LoanComponent',
    autoLoad: false,

    proxy: {
        type: 'rest',
        url: '/spyd/loans/{0}/loancomponents',
        extraParams: {
            adapter_id: 1
        },
        reader: {
            type: 'json',
            root: 'data'
        }
    }
});