/**
 * @class Spyd.store.Report
 * @extends Ext.data.Store
 * Report store class.
 */
Ext.define('Spyd.store.Report', {
    extend: 'Ext.data.Store',
    model: 'Spyd.model.Report',
    autoLoad: false,
    generateModel: false,
    reportUrl: '/spyd/view/reports',

    /**
     * Override constructor
     */
    constructor: function (config) {
        var proxy, model;
        Ext.apply(this, config);
        model = this.generateModel === true ? this.generateDynamicModel() : 'Spyd.model.Report';
        Ext.apply(this, {
            model: model,
            proxy: {
                type: 'rest',
                url: this.reportUrl,
                extraParams: {
                    adapter_id: 1
                },
                reader: {
                    type: 'json',
                    root: 'data'
                }
            }
        });
        this.callParent(arguments);
    },

    /**
     * Create dynamic model generation
     */
    generateDynamicModel: function () {
        var me = this,
            namespace = 'Spyd.model.' + me.storeId.toLowerCase() + '.ReportData';
        Ext.define(namespace, {
            extend: 'Ext.data.Model',
            fields: []
        });
        return namespace;
    }
});