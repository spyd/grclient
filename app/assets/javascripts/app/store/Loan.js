/**
 * @class Spyd.store.Loan
 * @extends Ext.data.Store
 * Loan store class.
 */
Ext.define('Spyd.store.Loan', {
    extend: 'Ext.data.Store',
    model: 'Spyd.model.Loan',
    autoLoad: false,

    proxy: {
        type: 'rest',
        url: '/spyd/loans',
        extraParams: {
            adapter_id: 1
        },
        reader: {
            type: 'json',
            root: 'data'
        }
    }
});