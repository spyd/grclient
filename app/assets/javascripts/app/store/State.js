/**
 * @class Spyd.store.State
 * @extends Ext.data.Store
 * State store class.
 */
Ext.define('Spyd.store.State', {
    extend: 'Ext.data.Store',
    model: 'Spyd.model.State',
    autoLoad: false,
    proxy: {
        type: 'rest',
        url: '/spyd/states',
        extraParams: {
            adapter_id: 1
        },
        reader: {
            type: 'json',
            root: 'data'
        }
    }
});