/**
 * @class Spyd.store.Reserve
 * @extends Ext.data.Store
 * Reserve store class.
 */
Ext.define('Spyd.store.Reserve', {
    extend: 'Ext.data.Store',
    model: 'Spyd.model.Reserve',
    autoLoad: false,

    proxy: {
        type: 'rest',
        url: '/spyd/reserves',
        extraParams: {
            adapter_id: 1
        },
        reader: {
            type: 'json',
            root: 'data'
        }
    }
});