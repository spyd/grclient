/**
 * @class Spyd.store.Component
 * @extends Ext.data.Store
 * Component store class.
 */
Ext.define('Spyd.store.Component', {
    extend: 'Ext.data.Store',
    model: 'Spyd.model.Component',
    autoLoad: false,
    accessoryId: -1,

    constructor: function (config) {
        var proxy;
        Ext.apply(this, config);
        pathUrl = config.url != null ? config.url : '/adapter/getData'
        proxy = this.generateProxy(pathUrl);
        Ext.apply(this, {
            proxy: proxy
        });
        this.callParent(arguments);
    },

    /**
     * Generates the store's proxy settings.
     */
    generateProxy: function (pathUrl) {
        var me = this;

        return {
            type: 'rest',
            url: pathUrl,
            extraParams: {
                adapter_id: 1,
                resource: '/spyd/accessories/' + me.accessoryId + '/components'
            },
            reader: {
                type: 'json',
                root: 'data'
            }
        };
    }
});