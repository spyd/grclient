/**
 * @class Spyd.store.Employee
 * @extends Ext.data.Store
 * Employee store class.
 */
Ext.define('Spyd.store.Employee', {
    extend: 'Ext.data.Store',
    model: 'Spyd.model.Employee',
    autoLoad: true,
    roleId: 2,

    constructor: function (config) {
        var proxy;
        Ext.apply(this, config);
        proxy = this.generateProxy();
        Ext.apply(this, {
            proxy: proxy
        });
        this.callParent(arguments);
    },

    /**
     * Generates the store's proxy settings.
     */
    generateProxy: function () {
        var me = this;

        return {
            type: 'rest',
            url: '/adapter/getData',
            extraParams: {
                id: 4,
                resource: '/spyd/roles/' + me.roleId + '/employees'
            },
            reader: {
                type: 'json',
                root: 'data'
            }
        };
    }
});
