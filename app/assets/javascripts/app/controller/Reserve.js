/**
 * @class Spyd.controller.Reserve
 * @extends Ext.app.Controller
 * Spyd Reserve controller.
 */
Ext.define('Spyd.controller.Reserve', {
    extend: 'Ext.app.Controller',

    views: [
        'Spyd.util.calendar.EventWindow',
        'Spyd.util.calendar.menu.Event'
    ],

    models: [
        'Accessory',
        'Reserve'
    ],

    stores: [
        'Accessory',
        'Reserve'
    ],

    init: function() {
        this.control({
            'extensible.eventcontextmenu': {
                confirmreserve: this.onConfirmreserve,
                cancelreserve: this.onCancelreserve
            },
            'extensible.eventeditwindow': {
                reserveAccessory: this.onReserveAccessory,
            }
        });
    },

    onReserveAccessory: function(panel) {
    },

    onConfirmreserve: function(panel, record, el) {
        console.log('confirmed');
    },

    onCancelreserve: function(panel, record, el) {
        console.log('canceled');
    }
});
