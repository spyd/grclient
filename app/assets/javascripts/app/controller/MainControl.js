/**
 * @class Spyd.controller.MainControl
 * @extends Ext.app.Controller
 * Spyd main controller.
 */
Ext.define('Spyd.controller.MainControl', {
    extend: 'Ext.app.Controller',

    views: [
        'MainControl',
        'MenuOption'
    ],

    selectedAccessory: null,

    init: function () {
        this.control({
            'controlgridview': {
                render: this.onPanelRendered,
                itemclick: this.onSelectAccessory,
                loanAccessoryAction: this.onLoanAccessory,
                returnAccessoryAction: this.onReturnAccessory
            },
            'mainpanel': {
                startUserSession: this.onOpenLoginSession
            },
            'barmenuoption button > [action=create-accessory]': {
                click: this.onCreateNewAccessory
            },
            'barmenuoption button > [action=edit-accessory]': {
                click: this.onEditSelectedAccessory
            },
            'barmenuoption button > [action=delete-accessory]': {
                click: this.onDeleteAccessory
            },
            'barmenuoption button > [action=list-accessory-view]': {
                click: this.onLoadAccessoryListView
            },
            'barmenuoption button > [action=reserve-accessory]': {
                click: this.onReserveAccessory
            },
            'barmenuoption button > [action=reports-accessory]': {
                click: this.onLoadReportsView
            },
            'barmenuoption > [action=logout-session]': {
                click: this.onCloseLogoutSession
            },
        });
    },

    onPanelRendered: function (panel) {
    },

    onLoadAccessoryListView: function (panel) {
        var mainAdminPnl = panel.up('#main-panel').down('#spyd-manage-card');
        mainAdminPnl.layout.setActiveItem(0);
    },

    onReserveAccessory: function (panel) {
        var me = this, mainAdminPnl = panel.up('#main-panel').down('#spyd-manage-card');
        me.loadAdminListReserves(mainAdminPnl, panel.up('#main-panel').userIdSession);
        mainAdminPnl.layout.setActiveItem(1);
    },

    onLoadReportsView: function (panel) {
        var mainAdminPnl = panel.up('#main-panel').down('#spyd-manage-card');
        mainAdminPnl.layout.setActiveItem(2);
    },

    onLoanAccessory: function (mainGridPanel, rowIndex) {
        var me = this, accesoryStore, componentStore, loanForm,
            selectedModel = mainGridPanel.selModel.getSelection()[0];
        if (selectedModel.index === rowIndex) {
            accesoryStore = Ext.create('Spyd.store.Accessory').load({
                scope: me,
                callback: function(accessoryStore) {
                    var selectedAccessory = null, message = '',
                        isNextHourReserved = false;
                    accessoryStore.forEach(function(item) {
                        if (selectedModel.data.AccessoryId === item.get('Id')) {
                            selectedAccessory = item;
                            return;
                        }
                    });
                    if (selectedModel.data.NextReserveHour !== '') {
                        isNextHourReserved = me.verifyNextReserve(selectedModel.data.NextReserveDate);
                        message = Ext.String.format('Aviso: El accesorio esta reservado a horas {0}', selectedModel.data.NextReserveHour);
                    }
                    
                    loanForm = Ext.create('Spyd.view.loan.LoanAccessoryForm', {
                        parentGridPanel: mainGridPanel,
                        accessoryModel: selectedAccessory,
                        nextHourReserved: isNextHourReserved
                    });
                    componentStore = Ext.create('Spyd.store.Component', {
                        accessoryId: selectedModel.data.AccessoryId,
                    });
                    loanForm.loadSelectedAccesoryforLend(selectedAccessory, componentStore, message);
                    loanForm.show();
                }
            });
        } else {
            Ext.Msg.alert('Préstamo Accesorio', 'Seleccione Accesorio para préstamo');
        }
    },

    verifyNextReserve: function(nextDate) {
        var isNextHourReserved = false,
            current = new Date();
        current.setMinutes(current.getMinutes() + 15);

        if (current.getHours() === nextDate.getHours() - 1 && current >= nextDate) {
            isNextHourReserved = true;
        }

        return isNextHourReserved;
    },

    onReturnAccessory: function (mainGridPanel, rowIndex) {
        var me = this, loanStore, accesoryStore, returnForm,
            selectedModel = mainGridPanel.selModel.getSelection()[0];
        if (selectedModel.index === rowIndex) {
            loanStore = Ext.create('Spyd.store.Loan').load({
                scope: me,
                callback: function(loanStore) {
                    var selectedLoanAccessory = null;
                    loanStore.forEach(function(item) {
                        if (selectedModel.data.LoanId === item.get('LoanId')) {
                            selectedLoanAccessory = item;
                            return;
                        }
                    });
                    
                    accesoryStore = Ext.create('Spyd.store.Accessory').load({
                        scope: me,
                        callback: function(accessoryStore) {
                            var selectedAccessory = null;
                            accessoryStore.forEach(function(item) {
                                if (selectedModel.data.AccessoryId === item.get('Id')) {
                                    selectedAccessory = item;
                                    return;
                                }
                            });
                            returnForm = Ext.create('Spyd.view.return.ReturnForm', {
                                parentGridPanel: mainGridPanel,
                                loanAccessoryModel: selectedLoanAccessory,
                                accessoryModel: selectedAccessory
                            });
                            returnForm.loadSelectedAccesoryforReturn(selectedLoanAccessory, selectedAccessory);
                            returnForm.show();
                        }
                    });
                }
            });
        } else {
            Ext.Msg.alert('Devolución Accesorio', 'Seleccione Accesorio para devolver');
        }
    },

    onCreateNewAccessory: function(panel) {
        var firstPnl = panel.up('#x-headerpanel').up(),
            mainGridPanel = firstPnl.down('#control-grid-view')
            accessoryForm = Ext.create('Spyd.view.accessory.AccessoryForm', {
                parentGridPanel: mainGridPanel
            });
        accessoryForm.show();
    },

    onSelectAccessory: function(panel, record) {
        var me = this;
        me.selectedAccessory = record;
    },

    onEditSelectedAccessory: function(panel) {
        var me = this, accessoryForm, accesoryStore, componentStore,
            firstPnl = panel.up('#x-headerpanel').up(),
            mainGridPanel = firstPnl.down('#control-grid-view');
        if (me.selectedAccessory !== null) {
            if (me.selectedAccessory.data.Status === 'Disponible'){
                accesoryStore = Ext.create('Spyd.store.Accessory').load({
                    scope: me,
                    callback: function(accessoryStore) {
                        var selectedAccessory = null;
                        accessoryStore.forEach(function(item) {
                            if (me.selectedAccessory.data.AccessoryId === item.get('Id')) {
                                selectedAccessory = item;
                                return;
                            }
                        });
                        componentStore = Ext.create('Spyd.store.Component', {
                            accessoryId: me.selectedAccessory.data.AccessoryId
                        });
                        accessoryForm = Ext.create('Spyd.view.accessory.AccessoryForm', {
                            parentGridPanel: mainGridPanel,
                            componentStore: componentStore,
                            accessoryModel: selectedAccessory,
                            editMode: true
                        });
                        accessoryForm.loadSelectedAccessory(selectedAccessory);
                        accessoryForm.show();
                    }
                });
            }else{
                Ext.Msg.alert('Editar Accesorio', 'El accesorio seleccionado esta en uso, por favor registre la devolución del accesorio antes de editarlo.');
            }
        } else {
            Ext.Msg.alert('Editar Accesorio', 'Seleccione Accesorio a ser editado');
        }
    },

    onDeleteAccessory: function(panel) {
        var me = this, accesoryStore,
            firstPnl = panel.up('#x-headerpanel').up(),
            mainGridPanel = firstPnl.down('#control-grid-view');
        if (me.selectedAccessory !== null) {
            if (me.selectedAccessory.data.Status === 'Disponible'){
                Ext.MessageBox.show({
                    title:'Borrar Accesorio',
                    msg: 'Está a punto de borrar el accesorio seleccionado. <br /> Esta seguro?',
                    buttons: Ext.MessageBox.YESNO,
                    buttonText:{ 
                        yes: "Si", 
                        no: "No" 
                    },
                    fn: function(btn) {
                        if (btn == "yes") {
                            accesoryStore = Ext.create('Spyd.store.Accessory').load({
                                scope: me,
                                callback: function(accessoryStore) {
                                    var selectedAccessory = null;
                                    accessoryStore.forEach(function(item) {
                                        if (me.selectedAccessory.data.AccessoryId === item.get('Id')) {
                                            selectedAccessory = item;
                                            return;
                                        }
                                    });
                                    selectedAccessory.set('Visible', false);
                                    selectedAccessory.save();
                                    mainGridPanel.getStore().load();
                                }
                            });
                        }
                    },
                    icon: Ext.MessageBox.QUESTION
                });
            }else{
                Ext.Msg.alert('Eliminar Accesorio', 'El accesorio seleccionado esta en uso, por favor registre la devolución del accesorio antes de eliminarlo.');
            }
        } else {
            Ext.Msg.alert('Eliminar Accesorio', 'Seleccione Accesorio a ser eliminado');
        }
    },

    onOpenLoginSession: function(cmp) {
        var loginPnl = cmp.getComponent('spyd-login-panel').getComponent('login-view').getComponent('login-panel'),
            userName = loginPnl.getComponent('username').getValue(),
            password = loginPnl.getComponent('password').getValue(),
            me = this, islogged = false;

        cmp.accountStore.each(function(account) {
            if (account.get('UserAccount').split('@')[0] === userName && account.get('Password') === password) {
                me.initializeUserSession(cmp, account);
                islogged = true;
                return;
            }
        });

        if (islogged === false) {
            Ext.Msg.alert('Información', 'El Nombre de Usuario y/o contraseña son incorrectos, <br />verifique los datos ingresados.');
        }
    },

    initializeUserSession: function(cmp, account) {
        var me = this, welcomePnl,
            store = Ext.create('Spyd.store.Employee', {roleId: account.get('RoleId')});

        store.load({
            scope: me,
            callback: function(employeeStore) {
                employeeStore.forEach(function(employee) {
                    if (account.get('EmployeeId') === employee.get('EmployeeId')) {
                        cmp.userIdSession = account.get('EmployeeId');
                        cmp.userNameSession = employee.get('FullName');
                        if (account.get('RoleId') === 3) {
                            welcomePnl = cmp.layout.getLayoutItems()[1].down('#user-info');
                            welcomePnl.items.getAt(0).setText(Ext.String.format('Bienvenido a Spyd : {0}', employee.get('FullName')));
                            me.loadEmployeeReserves(cmp.layout.getLayoutItems()[1], account.get('EmployeeId'));
                            cmp.layout.setActiveItem(1);
                        } else {
                            welcomePnl = cmp.layout.getLayoutItems()[2].down('#user-info');
                            welcomePnl.items.getAt(0).setText(Ext.String.format('Bienvenido a Spyd : {0}', employee.get('FullName')));
                            cmp.layout.setActiveItem(2);
                        }
                        return;
                    }
                });
            }
        });
    },

    loadEmployeeReserves: function(panel, employeeId) {
        var searchListPanel = panel.down('#search-accessory-list'),
            calendarPanel = panel.down('#my-reserve-list').down('#reserve-calendar');
        calendarPanel.loadMyReservesCalendar(searchListPanel.store, employeeId);
        //searchListPanel.store.load();
        Ext.getCmp('spyd-main-panel').adminMode = false;
    },

    loadAdminListReserves: function(panel, employeeId) {
        var searchListPanel = panel.down('#manage-reserve-list').down('#search-accessory-list'),
            calendarPanel = panel.down('#manage-reserve-list').down('#reserve-calendar');
        calendarPanel.loadListReservesCalendar(searchListPanel.store, employeeId);
        Ext.getCmp('spyd-main-panel').adminMode = true;
    },

    onCloseLogoutSession: function(cmp) {
        var mainPnl = cmp.up('#main-panel'),
            mainAdminPnl = mainPnl.down('#spyd-manage-card');

        mainPnl.layout.setActiveItem(0);
        mainPnl.layout.getActiveItem().down('#username').setValue('');
        mainPnl.layout.getActiveItem().down('#password').setValue('');
        mainPnl.down('#reserve-form').layout.setActiveItem(0);
        mainAdminPnl.layout.setActiveItem(0);
        window.location.reload();
    }
});