/**
 * @class Spyd.controller.LoanAccessory
 * @extends Ext.app.Controller
 * Spyd LoanAccessory controller.
 */
Ext.define('Spyd.controller.LoanAccessory', {
    extend: 'Ext.app.Controller',

    views: [
        'loan.LoanAccessoryForm', 
    ],

    models: [
        'Loan',
        'LoanComponent'
    ],

    stores: [
        'Loan',
        'LoanComponent'
    ],

    init: function () {
        this.control({
            'lendingmanager': {
                render: this.onPanelRendered,
                createLoanAccessory: this.onLoanAccessory
            },
            'returnmanager': {
                returnLoanAccessory: this.onReturnLoanAccessory
            }
        });
    },

    onPanelRendered: function(panel) {
    },

    onLoanAccessory: function(panel) {
        var me = this, accesoryStore, componentStore,
            loanForm = panel.down('#pnl-lending'),
            gridComponents = panel.down('#pnl-loangrid-items'),
            currentDate = new Date(),
            loanAccessory = Ext.create('Spyd.model.Loan', {
                ResponsibleId: loanForm.items.get('responsibleId').getValue(),
                EmployeeId: loanForm.items.get('employeeId').getValue(),
                AccessoryId: panel.accessoryModel.get('Id'),
                LoanDate: loanForm.items.get('enterdateId').getValue(),
                LoanHour: Ext.Date.format(currentDate, 'H:i:s'),
                ReturnDate: null,
                ReturnHour: '',
                Comments: ''
            });

        loanAccessory.save({
            success: function (record) {
                var index = 0;
                gridComponents.getStore().each(function(item) {
                    if (item.get('IsLoaned') === true) {
                        var component = Ext.create('Spyd.model.LoanComponent', {
                            LoanId: record.get('LoanId'),
                            ComponentId: item.get('ComponentId'),
                            AccessoryId: item.get('AccessoryId'),
                            LoanAmount: item.get('Amount'),
                            IsLoaned: true
                        });
                        component.proxy.url = Spyd.util.Util.formatArguments('/spyd/loans/{0}/loancomponents', [record.data.LoanId]);
                        component.cmpIndex = index++;
                        component.save({
                            success: function (cmpRecord) {
                                var itemCmp = gridComponents.getStore().data.items[this.cmpIndex];
                                cmpRecord.set('LoanId', record.get('LoanId'));
                                cmpRecord.set('ComponentId', itemCmp.get('ComponentId'));
                                cmpRecord.set('AccessoryId', itemCmp.get('AccessoryId'));
                                cmpRecord.set('LoanAmount', itemCmp.get('Amount'));
                                cmpRecord.save();
                            }
                        });
                    } else {
                        index++;
                    }
                });
                panel.accessoryModel.set('StatusId', 2);
                panel.accessoryModel.save();
                panel.parentGridPanel.getStore().load();
            }
        });
    },

    onReturnLoanAccessory: function(panel) {
        var me = this, accesoryStore, componentStore,
            returnForm = panel.down('#pnl-return'),
            gridComponents = panel.down('#pnl-returngrid-items'),
            currentDate = new Date();

        panel.loanAccessoryModel.set('ReturnDate', returnForm.items.get('returndateId').getValue());
        panel.loanAccessoryModel.set('ReturnHour', Ext.Date.format(currentDate, 'H:i:s'));
        panel.loanAccessoryModel.set('Comments', returnForm.items.get('commetsId').getValue());
        panel.loanAccessoryModel.save({
            success: function (record) {
                gridComponents.getStore().each(function(item) {
                    var cmpRecord =  panel.loanComponentStore.findRecord('LoanComponentId', item.data.LoanComponentId);
                    cmpRecord.set('ReturnAmount', item.get('ReturnAmount'));
                    cmpRecord.proxy.url = Spyd.util.Util.formatArguments('/spyd/loans/{0}/loancomponents', [item.data.LoanId]);
                    cmpRecord.save();
                });

                panel.accessoryModel.set('StatusId', 1);
                panel.accessoryModel.save();
                panel.parentGridPanel.getStore().load();
            }
        });
    }
});