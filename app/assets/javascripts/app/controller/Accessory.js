/**
 * @class Spyd.controller.Accessory
 * @extends Ext.app.Controller
 * Spyd Accessory controller.
 */
Ext.define('Spyd.controller.Accessory', {
    extend: 'Ext.app.Controller',

    views: [
        'accessory.AccessoryForm', 
    ],

    models: [
        'Accessory',
        'Component'
    ],

    stores: [
        'Accessory',
        'Component'
    ],

    init: function () {
        this.control({
            'accessorymanager': {
                render: this.onPanelRendered,
                createAccessory: this.onCreateAccessory,
                editAccessory: this.onEditAccessory
            }
        });
    },

    onPanelRendered: function (panel) {
    },

    onCreateAccessory: function (panel) {
        var me = this, accesoryStore, componentStore, accessoryStatus,
            accessoryForm = panel.down('#pnl-accessory'),
            accessoryImage = panel.down('#preview-image-btn'),
            gridComponents = panel.down('#pnl-grid-items'),
            accessory = Ext.create('Spyd.model.Accessory', {
                Name: Ext.util.Format.trim(accessoryForm.items.get('nameId').getValue()),
                Description: accessoryForm.items.get('descriptionId').getValue(),
                ImagePath: accessoryImage.getValue() !== null ? accessoryImage.getValue().split('\\')[2] : 'sample-accessory.jpg',
                AcquisitionDate: accessoryForm.items.get('enterdateId').getValue(),
                EmployeeId: accessoryForm.items.get('ownerId').getValue(),
                StatusId: 1,
                Visible: true
            }); 

        accessory.save({
            success: function (record) {
                var index = 0;
                gridComponents.getStore().each(function(item) {
                    var component = Ext.create('Spyd.model.Component', {
                        ComponentName: item.get('ComponentName'),
                        Amount: item.get('Amount'),
                        AccessoryId: record.data.Id
                    });
                    component.proxy.url = Spyd.util.Util.formatArguments(component.proxy.url, [record.data.Id]);
                    component.cmpIndex = index++;
                    component.save({
                        success: function (cmpRecord) {
                            cmpRecord.set('ComponentName', gridComponents.getStore().data.items[this.cmpIndex].get('ComponentName'));
                            cmpRecord.set('Amount', gridComponents.getStore().data.items[this.cmpIndex].get('Amount'));
                            cmpRecord.save();
                        }
                    });
                });
                panel.parentGridPanel.getStore().load();
            }
        });
    },

    onEditAccessory: function (panel) {
        var me = this, accesoryStore, componentStore, accessoryStatus,
            accessoryForm = panel.down('#pnl-accessory'),
            accessoryImage = panel.down('#preview-image-btn'),
            gridComponents = panel.down('#pnl-grid-items'),
            imagePath = accessoryImage.getValue() !== null ? accessoryImage.getValue().split('\\')[2] : 'sample-accessory.jpg';

        panel.accessoryModel.set('Name', accessoryForm.items.get('nameId').getValue());
        panel.accessoryModel.set('Description', accessoryForm.items.get('descriptionId').getValue());
        panel.accessoryModel.set('ImagePath', imagePath);
        panel.accessoryModel.set('AcquisitionDate', accessoryForm.items.get('enterdateId').getValue());
        panel.accessoryModel.set('EmployeeId', accessoryForm.items.get('ownerId').getValue());
        panel.accessoryModel.set('StatusId', 1);

        panel.accessoryModel.save();
        panel.componentStore.proxy.url = '/spyd/accessories/' + panel.accessoryModel.get('Id') + '/components';
        panel.componentStore.sync();
        panel.parentGridPanel.getStore().load();
    },

    updateAccessoryComponents: function (componentStore){
    }
});