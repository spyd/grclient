Spyd::Application.routes.draw do
  match '/adapter/getData' => 'adapters#data'
  root :to => 'adapters#index'
  get '*resource' => 'adapters#get'
  post '*resource' => 'adapters#post'
  put '*resource' => 'adapters#put'
  delete '*resource' => 'adapters#delete'
end
