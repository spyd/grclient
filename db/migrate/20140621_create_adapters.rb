class CreateAdapters < ActiveRecord::Migration
  def change
    create_table :adapters do |t|
      t.string :host
      t.integer :port
      t.string :name
      t.string :description

      t.timestamps
    end
  end
end
